# coding=utf-8
import re

from django.core.validators import RegexValidator
from django.db import models, transaction

from empreses.models import EmpresaAsseguradora, Cooperativa, Empresa
from socies.models import Persona, AdrecaProjecteAutoocupat, Activitat
from alta_socies.models import ProcesAltaAutoocupat, PeticioAsseguranca


class ProjecteAutoocupatManager(models.Manager):

    @transaction.atomic
    def create_projecte(self, proces):

        projecte = self.create(proces=proces)

        projecte.nom = proces.nom
        projecte.email = proces.email
        projecte.telefon = proces.telefon
        projecte.website = proces.website
        projecte.descripcio = proces.descripcio
        projecte.data_creacio = proces.data_resolucio
        projecte.tipus = proces.tipus
        projecte.individual = proces.individual
        projecte.es_paic = proces.es_paic
        projecte.te_comerc_electronic = proces.te_comerc_electronic
        projecte.mercat_virtual = proces.mercat_virtual
        projecte.te_productes_ecologics = proces.te_productes_ecologics
        projecte.tipus_parada_firaire = proces.tipus_parada_firaire
        projecte.expositors = proces.expositors
        projecte.compte_ces_assignat = proces.compte_CES_assignat
        projecte.iva_assignat = proces.iva_assignat
        projecte.quota_avancada_quantitat = proces.quota_avancada_quantitat
        projecte.quota_avancada_forma_pagament = proces.quota_avancada_forma_pagament

        projecte.cooperativa_assignada = proces.cooperativa_assignada

        for membre in proces.membres_de_referencia.all():
            membre.crear_usuaria_si_cal()
            projecte.membres_de_referencia.add(membre)
        for adreca in proces.adreces.all():
            projecte.adreces.add(adreca)
        for activitat in proces.altres_activitats.all():
            projecte.altres_activitats.add(activitat)
        for soci in proces.socies_afins_addicionals.all():
            projecte.socies_afins_addicionals.add(soci)

        projecte.socia_mentora = proces.socia_mentora

        projecte.save()

        return projecte


class ProjecteAutoocupat(models.Model):

    validator_compte_ces_assignat = RegexValidator(regex=re.compile(r'^COOP\d{4}$'),
                                                   message=u"el format de compte_ces_assignat ha de ser COOP9999")

    proces = models.OneToOneField(ProcesAltaAutoocupat, null=True, on_delete=models.SET_NULL)

    nom = models.CharField(max_length=256)
    email = models.EmailField()
    telefon = models.CharField(max_length=32)
    website = models.URLField(verbose_name=u"lloc web", null=True)
    descripcio = models.TextField(verbose_name=u"descripció", null=True)
    data_creacio = models.DateField(null=True)
    tipus = models.CharField(max_length=20)
    individual = models.CharField(max_length=16)
    es_paic = models.BooleanField(default=False)
    te_comerc_electronic = models.BooleanField(default=False)
    mercat_virtual = models.BooleanField(default=False)
    te_productes_ecologics = models.BooleanField(default=False)
    tipus_parada_firaire = models.CharField(max_length=16)
    expositors = models.BooleanField(default=False)
    compte_ces_assignat = models.CharField(max_length=16, unique=True, validators=[validator_compte_ces_assignat])
    iva_assignat = models.IntegerField(default=18)
    quota_avancada_quantitat = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    quota_avancada_forma_pagament = models.CharField(max_length=6)

    cooperativa_assignada = models.ForeignKey(Cooperativa, null=True)

    membres_de_referencia = models.ManyToManyField(Persona,
                                                   related_name='projectes_autoocupats',
                                                   verbose_name=u"membres de referència")
    adreces = models.ManyToManyField(AdrecaProjecteAutoocupat,
                                     related_name='xxx_projecte_autoocupat')  # TODO remove when fixed upstream
    altres_activitats = models.ManyToManyField(Activitat, related_name='+',
                                               help_text=u"Altres activitats no vinculades a cap adreça")
    socies_afins_addicionals = models.ManyToManyField(Persona, related_name='+')

    socia_mentora = models.OneToOneField(Persona, null=True, related_name='+')

    objects = ProjecteAutoocupatManager()

    def __unicode__(self):
        return u"%s: %s" % (self.compte_ces_assignat, self.nom)

    @staticmethod
    def _filtrar_queryset_factura_per_data(queryset, des_de, fins_a):
        """
        donat un queryset sobre el model Factura, retorna un nou queryset que exclou
        les factures que es troben fora del periode que indiquen les dates.
        :type queryset: django.db.models.query.QuerySet
        :param queryset: hauria de ser sobre el model Factura, o "derivats"
        :param des_de: data inicial del periode a seleccionar
        :param fins_a: data final del periode a seleccionar
        :return: queryset original modificat de manera que exclou factures amb data < "des_de" i > "fins_a"
        """
        if des_de:
            queryset = queryset.filter(data__gte=des_de)
        if fins_a:
            queryset = queryset.filter(data__lte=fins_a)
        return queryset

    def factures_rebudes(self, des_de=None, fins_a=None):
        from facturacio.models import FacturaRebuda
        factures = FacturaRebuda.objects.filter(projecte=self)
        factures = self._filtrar_queryset_factura_per_data(factures, des_de, fins_a)
        factures = factures.exclude(proveidor=self.cooperativa_assignada)  # TODO això assumeix que la coop no canvia!!
        return factures

    def factures_emeses(self, des_de=None, fins_a=None):
        from facturacio.models import FacturaEmesa
        factures = FacturaEmesa.objects.filter(projecte=self)
        factures = self._filtrar_queryset_factura_per_data(factures, des_de, fins_a)
        factures = factures.exclude(client=self.cooperativa_assignada)  # TODO això assumeix que la coop no canvia!!
        return factures

    def proveidors(self, des_de=None, fins_a=None):
        # TODO es podria limitar per defecte a p. ex. les factures de l'any acutal + l'anterior
        factures = self.factures_rebudes(des_de, fins_a)
        llista_id_proveidors = factures.values_list('proveidor', flat=True).distinct()
        return Empresa.objects.filter(id__in=llista_id_proveidors)

    def clients(self, des_de=None, fins_a=None):
        # TODO es podria limitar per defecte a p. ex. les factures de l'any acutal + l'anterior
        factures = self.factures_emeses(des_de, fins_a)
        llista_id_clients = factures.values_list('client', flat=True).distinct()
        return Empresa.objects.filter(id__in=llista_id_clients)

    class Meta:
        verbose_name = u"projecte autoocupat"
        verbose_name_plural = u"projectes autoocupats"


class PolissaAssegurancaManager(models.Manager):

    def create_polissa(self, peticio, projecte):

        polissa = self.create(peticio=peticio, projecte_autoocupat=projecte)

        polissa.adreca = peticio.adreca
        polissa.activitat = peticio.activitat
        polissa.comentaris_avaluacio = peticio.comentaris_avaluacio
        polissa.companyia_asseguradora = peticio.companyia_asseguradora
        polissa.numero_polissa = peticio.numero_polissa
        polissa.data_inici_polissa = peticio.data_inici_polissa
        polissa.data_final_polissa = peticio.data_final_polissa
        polissa.import_polissa = peticio.import_polissa
        polissa.comentaris_alta = peticio.comentaris_alta

        polissa.save()

        return polissa


class PolissaAsseguranca(models.Model):

    peticio = models.OneToOneField(PeticioAsseguranca, null=True, on_delete=models.SET_NULL)

    adreca = models.ForeignKey(AdrecaProjecteAutoocupat, null=True)
    activitat = models.ForeignKey(Activitat, null=True)
    projecte_autoocupat = models.ForeignKey(ProjecteAutoocupat, related_name='polisses_asseguranca')
    comentaris_avaluacio = models.TextField(null=True)

    companyia_asseguradora = models.ForeignKey(EmpresaAsseguradora, null=True)
    numero_polissa = models.CharField(max_length=128, verbose_name=u"número de pòlissa")
    data_inici_polissa = models.DateField(null=True, verbose_name=u"data d'inici de la cobertura")
    data_final_polissa = models.DateField(null=True, verbose_name=u"data d'acabament de la cobertura")
    import_polissa = models.DecimalField(max_digits=8, decimal_places=2, null=True,
                                         verbose_name=u"import total periode cobertura")
    comentaris_alta = models.TextField(null=True)

    objects = PolissaAssegurancaManager()

    class Meta:
            verbose_name = u"pòlissa d'assegurança"
            verbose_name_plural = u"pòlisses d'assegurança"


class FotoProjecteAutoocupatFiraire(models.Model):

    projecte = models.ForeignKey(ProjecteAutoocupat, related_name='fotos_parada_firaire')
    nom = models.CharField(max_length=256)
    detalls = models.TextField(blank=True, null=True)
    imatge = models.ImageField(upload_to='fotos_projecte_autoocupat_firaire')
    url_miniatura = models.CharField(max_length=1024, blank=True, null=True)
