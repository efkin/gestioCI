from django.contrib import admin

from .models import AdrecaProjecteAutoocupat, Activitat, Persona, ContribucioCIC, Habilitat, GrupHabilitat, \
    HabilitatsContribucionsPersona, AdrecaFiscal

for model in (AdrecaProjecteAutoocupat,
              AdrecaFiscal,
              Activitat,
              Persona,
              HabilitatsContribucionsPersona,
              ContribucioCIC,
              Habilitat,
              GrupHabilitat):
    admin.site.register(model)
