from django.contrib import admin
from .models import ProcesAltaAutoocupat, SessioAvaluacio, SessioMoneda, SessioAcollida, \
                    SessioAlta, Ubicacio, SociaCooperativa, ProjecteCollectiu, SociaAfi


class SessioAdmin(admin.ModelAdmin):
    list_display = ('data', 'ubicacio', 'responsable_alta')
    date_hierarchy = 'data'


admin.site.register(SessioAcollida, SessioAdmin)
# admin.site.register(SessioAvaluacio, SessioAdmin)  # de moment no es fa servir
admin.site.register(SessioMoneda, SessioAdmin)
# admin.site.register(SessioAlta, SessioAdmin)  # de moment no es fa servir

for model in (ProcesAltaAutoocupat, Ubicacio, SociaCooperativa,
              ProjecteCollectiu, SociaAfi):
    admin.site.register(model)
