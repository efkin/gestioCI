# coding=utf-8
import re
from collections import OrderedDict

from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.db import models, transaction
from django.utils import timezone

from empreses.models import EmpresaAsseguradora, Cooperativa
from socies.models import Persona, AdrecaProjecteAutoocupat, Adreca, Activitat, AdrecaProjecteCollectiu
from gestioci.formats import format_data_i_hora


class Ubicacio(Adreca):

    nom = models.CharField(max_length=256, unique=True)

    class Meta:
        verbose_name = u"ubicació"
        verbose_name_plural = u"ubicacions"

    def __unicode__(self):
        return self.nom


class Sessio(models.Model):

    data = models.DateTimeField()
    ubicacio = models.ForeignKey(Ubicacio)
    duracio = models.PositiveSmallIntegerField(verbose_name=u"Duració", help_text=u"En minuts")
    responsable_alta = models.ForeignKey(User)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-data']

    def is_in_future(self):
        """ returns true if the date is in the future """
        ara = timezone.localtime(timezone.now())
        delta = ara - self.data
        seconds = delta.total_seconds()
        return seconds < 0

    def __unicode__(self):
        return u"%s %s (%s)" % (format_data_i_hora(self.data), self.ubicacio, self.responsable_alta.first_name)


class SessioAcollida(Sessio):
    """
    Sessió d'acollida; en grup, oberta
    """

    class Meta:
        verbose_name = u"sessió d'acollida"
        verbose_name_plural = u"sessions d'acollida"


class SessioAvaluacio(Sessio):
    """
    Sessió d'avaluació; privada per cada projecte; amb cita prèvia
    """

    class Meta:
        verbose_name = u"sessió d'avaluació"
        verbose_name_plural = u"sessions d'avaluació"


class SessioMoneda(Sessio):
    """
    Sessió de formació sobre "Moneda Social"; en grup, oberta
    """

    class Meta:
        verbose_name = u"sessió de moneda social"
        verbose_name_plural = u"sessions de moneda social"


class SessioAlta(Sessio):
    """
    Sessió d'alta; privada per cada projecte; amb cita prèvia
    """

    class Meta:
        verbose_name = u"sessió d'alta"
        verbose_name_plural = u"sessions d'alta"


class ProcesAltaAutoocupat(models.Model):
    """
    Modela el procés d'alta d'un projecte autoocupat.
    Acumula la informació que es va recollint durant el procés.
    Acabarà creant les entitats que correspongui.

    WORKFLOW:
        1. Sessió d'acollida (en grup)
        2. Sessió d'avaluació (individual)
        3. Sessió de moneda social (en grup)
        4. Sessió d'alta (individual)
    """

    PASSOS = {
        0: {'nom':        u"Sessió d'acollida: identificació del projecte",
            'template':   'acollida-identificacio_projecte',
            },
        1: {'nom':        u"Sessió d'acollida: Assistents (membres de referència)",
            'template':   'acollida-membres_de_referencia',
            },
        2: {'nom':        u"Sessió d'avaluació: dades generals del projecte",
            'template':   'avaluacio-dades_generals_projecte',
            },
        3: {'nom':        u"Sessió d'avaluació: detalls dels membres de referència",
            'template':   'avaluacio-detalls_socis_de_referencia',
            },
        4: {'nom':        u"Sessió d'avaluació: adreces a on es desenvolupa l'activitat",
            'template':   'avaluacio-adreces',
            },
        5: {'nom':        u"Sessió d'avaluació: altres activitats no vinculades a cap adreça",
            'template':   'avaluacio-altres-activitats',
            },
        6: {'nom':        u"Sessió d'avaluació: concessió d'activitats",
            'template':   'avaluacio-concessio-activitats',
            },
        7: {'nom':        u"Sessió d'avaluació: assegurances d'activitats i adreces",
            'template':   'avaluacio-assegurances',
            },
        8: {'nom':        u"Sessió d'avaluació: forma de pagament per les quotes inicials",
            'template':   'avaluacio-quotes',
            },
        9: {'nom':        u"Sessió d'avaluació: enviament de la llista de tasques pendents",
            'template':   'avaluacio-tasques_pendents',
            },
        10: {'nom':       u"Sessió de moneda social",
             'template':  'moneda_social-assistencia',
            },
        11: {'nom':       u"Sessió d'alta: cessions d'ús a la CIC",
             'template':  'alta-cessions_us',
             },
        12: {'nom':       u"Sessió d'alta: lloguers a nom de la CIC",
             'template':  'alta-lloguers',
             },
        13: {'nom':       u"Sessió d'alta: concessió d'activitats",
             'template':  'alta-concessio_activitats',
             },
        14: {'nom':       u"Sessió d'alta: assegurances",
             'template':  'alta-assegurances',
             },
        15: {'nom':       u"Sessió d'alta: quotes",
             'template':  'alta-quotes',
             },
        16: {'nom':       u"Sessió d'alta: sòcies afins",
             'template':  'alta-socies_afins',
             },
        17: {'nom':       u"Sessió d'alta: sòcia mentora",
             'template':  'alta-mentora',
             },
        18: {'nom':       u"Sessió d'alta: fotografies del projecte",
             'template':  'alta-fotos_projecte_firaire',
             },
        19: {'nom':       u"Sessió d'alta: activació del projecte",
             'template':  'alta-activacio',
             },
        20: {'nom':       u"Sessió d'alta: accés a l'aplicació",
             'template':  'alta-acces_portal_socia',
             },
    }

    RESOLUCIO_EN_CURS = 'en_curs'
    RESOLUCIO_ACCEPTAT = 'acceptat'
    RESOLUCIO_REBUTJAT = 'rebutjat'
    RESOLUCIONS = (
        (RESOLUCIO_EN_CURS, u"en curs"),
        (RESOLUCIO_ACCEPTAT, u"acceptat"),
        (RESOLUCIO_REBUTJAT, u"rebutjat"),
    )

    TIPUS_AUTOOCUPAT = 'autoocupat'
    TIPUS_AUTOOCUPAT_FIRAIRE = 'autoocupat_firaire'
    TIPUS_PAIC_FACTURACIO = 'paic_amb_facturacio'
    TIPUS = (
        (TIPUS_AUTOOCUPAT, u"autoocupat"),
        (TIPUS_AUTOOCUPAT_FIRAIRE, u"autoocupat firaire"),
        (TIPUS_PAIC_FACTURACIO, u"PAIC amb facturació"),
    )

    INDIVIDUAL_INDIVIDUAL = 'individual'
    INDIVIDUAL_COL_LECTIU = 'col.lectiu'
    INDIVIDUAL = (
        (INDIVIDUAL_INDIVIDUAL, u"individual"),
        (INDIVIDUAL_COL_LECTIU, u"col·lectiu")
    )

    PARADA_FIRAIRE_NO = 'no'
    PARADA_FIRAIRE_METALL = 'metall'
    PARADA_FIRAIRE_FUSTA = 'fusta'
    PARADA_FIRAIRE = (
        (PARADA_FIRAIRE_NO, u"no en té"),
        (PARADA_FIRAIRE_FUSTA, u"de fusta"),
        (PARADA_FIRAIRE_METALL, u"metàl·lica"),
    )

    FORMA_PAGAMENT_EURO = 'euro'
    FORMA_PAGAMENT_ECO = 'eco'
    FORMA_PAGAMENT_HORA = 'h'

    FORMES_PAGAMENT_QUOTA_ALTA = (
        (FORMA_PAGAMENT_ECO, u"ecos"),
        (FORMA_PAGAMENT_EURO, u"euros"),
        (FORMA_PAGAMENT_HORA, u"hores"),
    )

    FORMES_PAGAMENT_QUOTA_AVANCADA = (
        (FORMA_PAGAMENT_ECO, u"ecos"),
        (FORMA_PAGAMENT_EURO, u"euros"),
    )

    QUOTA_ALTA = 'alta'
    QUOTA_TRIMESTRAL_AVANCADA = 'trimestral_avancada'
    QUOTES = (
        (QUOTA_ALTA, u"quota d'alta"),
        (QUOTA_TRIMESTRAL_AVANCADA, u"quota trimestral avançada")
    )

    # TODO les opcions d'IVA haurien d'estar a la base de dades;
    # potser tb en forma d'històric amb data d'entrada i sortida de vigor, etc.
    OPCIONS_IVA = (
        (0, u"0% (exempt)"),
        (4, u"4% (super-reduït)"),
        (8, u"8% (reduït)"),
        (18, u"18% (general)")
    )

    validator_compte_ces_assignat = RegexValidator(regex=re.compile(r'^(COOP\d{4})?$'),
                                                   message=u"el format de compte_ces_assignat ha de ser COOP9999")

    # dades del projecte
    nom = models.CharField(max_length=256)
    email = models.EmailField()
    telefon = models.CharField(max_length=32)
    website = models.URLField(verbose_name=u"lloc web", blank=True, null=True)
    descripcio = models.TextField(verbose_name=u"descripció", blank=True, null=True)

    membres_de_referencia = models.ManyToManyField(Persona, blank=True,
                                                   related_name='altes_projecte_autoocupat',
                                                   verbose_name=u"membres de referència")

    sessio_acollida = models.ForeignKey(SessioAcollida, related_name='altes_projecte_autoocupat',
                                        verbose_name=u"sessió d'acollida")
    sessio_acollida_realitzada = models.BooleanField(default=False)
    sessio_avaluacio = models.ForeignKey(SessioAvaluacio, null=True, blank=True,
                                         related_name='alta_projecte_autoocupat')
    sessio_avaluacio_realitzada = models.BooleanField(default=False)
    sessio_moneda = models.ForeignKey(SessioMoneda, null=True, blank=True, related_name='altes_projecte_autoocupat')
    sessio_moneda_realitzada = models.BooleanField(default=False)
    sessio_alta = models.ForeignKey(SessioAlta, null=True, blank=True, related_name='alta_projecte_autoocupat')
    sessio_alta_realitzada = models.BooleanField(default=False)

    pas = models.SmallIntegerField(default=0, verbose_name=u"pas al procés d'alta",
                                   help_text=u"punt del procés d'alta en el que es troba el projecte")
    pas_maxim_assolit = models.SmallIntegerField(default=0, verbose_name=u"pas més avançat que s'ha assolit")
    resolucio = models.CharField(max_length=16, choices=RESOLUCIONS, default=RESOLUCIO_EN_CURS)
    data_resolucio = models.DateTimeField(blank=True, null=True)
    comentaris_resolucio = models.TextField(blank=True)
    resolucio_usuaria = models.ForeignKey(User, null=True, blank=True, related_name='+')

    responsable = models.ForeignKey(User, help_text=u"Últim responsable d'alta que ha modificat el procés")

    tipus = models.CharField(max_length=20, default=TIPUS_AUTOOCUPAT, choices=TIPUS)
    individual = models.CharField(max_length=16, default=INDIVIDUAL_INDIVIDUAL, choices=INDIVIDUAL)

    es_paic = models.BooleanField(default=False)
    te_comerc_electronic = models.BooleanField(default=False, verbose_name=u"té comerç electrònic")
    mercat_virtual = models.BooleanField(default=False)

    te_productes_ecologics = models.BooleanField(default=False, verbose_name=u"té productes ecològics")
    tipus_parada_firaire = models.CharField(max_length=16, choices=PARADA_FIRAIRE, default=PARADA_FIRAIRE_NO)
    expositors = models.BooleanField(default=False, verbose_name=u"té expositors a AureaSocial")

    adreces = models.ManyToManyField(AdrecaProjecteAutoocupat, blank=True,
                                     related_name='xxx_proces_alta_autoocupat')  # TODO remove when fixed upstream
    altres_activitats = models.ManyToManyField(Activitat, blank=True,
                                               help_text=u"Altres activitats no vinculades a cap adreça")

    # ja son sòcies afins les persones «membres de referència»
    socies_afins_addicionals = models.ManyToManyField(Persona, blank=True, related_name='+')
    socia_mentora = models.OneToOneField(Persona, blank=True, null=True, related_name='+')

    compte_CES_assignat = models.CharField(max_length=16, blank=True,
                                           validators=[validator_compte_ces_assignat],
                                           null=True)  # TODO s'hauria de dir integralces per coherència?
    cooperativa_assignada = models.ForeignKey(Cooperativa, blank=True, null=True)
    iva_assignat = models.IntegerField(default=18)

    # QUOTA D'ALTA
    quota_alta_quantitat = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    quota_alta_forma_pagament = models.CharField(max_length=6,
                                                 choices=FORMES_PAGAMENT_QUOTA_ALTA,
                                                 default=FORMA_PAGAMENT_EURO)
    quota_alta_data_hora_pagament = models.DateTimeField(blank=True, null=True)

    # QUOTA TRIMESTRAL AVANÇADA
    quota_avancada_quantitat = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    quota_avancada_forma_pagament = models.CharField(max_length=6,
                                                     choices=FORMES_PAGAMENT_QUOTA_AVANCADA,
                                                     default=FORMA_PAGAMENT_EURO)
    quota_avancada_data_hora_pagament = models.DateTimeField(blank=True, null=True)

    # DETALLS COMUNS A TOTES DUES QUOTES
    compte_CES_que_paga_les_quotes = models.CharField(max_length=16, blank=True, null=True)
    pagament_quotes_numero_transaccio = models.CharField(max_length=128, blank=True, null=True)
    comentaris_pagament_quotes = models.TextField(max_length=1024, blank=True, null=True,
                                                  verbose_name=u"Comentaris al moment del pagament de quotes")

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = u"procés d'alta de projecte autoocupat"
        verbose_name_plural = u"processos d'alta de projecte autoocupat"

    # TODO això sembla un truc que no hauria d'estar aqui, diria que es una resta d'alguna sessió de debugging
    def save(self, *args, **kwargs):
        self.actualitza_quotes()
        super(ProcesAltaAutoocupat, self).save(*args, **kwargs)

    def __unicode__(self):
        # TODO problema decoding unicode string a una excepció - ve d'aquí!!
        # sona absurd que peti l'aplicació pel fet que la funcio __unicode__() retorni
        # caracters unicode als literals delimitats per u""
        return u"[%s] procés per la promoció a autoocupat de \"%s\"" % (str(self.id), self.nom)

    def _quotes(self):

        # TODO aquesta taula hauria d'estar a la base de dades!
        # columna «data d'entrada en vigor»
        # columna «individual o col·lectiu»
        # columna «quota» (alta, trimestral_avancada)
        # columna «forma de pagament»
        # - Aquí es triaria la fila que correspon a self.«data prevista d'alta del projecte»

        return {
            self.INDIVIDUAL_INDIVIDUAL: {
                self.QUOTA_ALTA: (
                    (self.FORMA_PAGAMENT_EURO,       30),
                    (self.FORMA_PAGAMENT_ECO,        30),
                    (self.FORMA_PAGAMENT_HORA,        6),
                ),
                self.QUOTA_TRIMESTRAL_AVANCADA: (
                    (self.FORMA_PAGAMENT_EURO,       45),
                    (self.FORMA_PAGAMENT_ECO,        45),
                ),
            },
            self.INDIVIDUAL_COL_LECTIU: {
                self.QUOTA_ALTA: (
                    (self.FORMA_PAGAMENT_EURO,       60),
                    (self.FORMA_PAGAMENT_ECO,        60),
                    (self.FORMA_PAGAMENT_HORA,       12),
                ),
                self.QUOTA_TRIMESTRAL_AVANCADA: (
                    (self.FORMA_PAGAMENT_EURO,       45),
                    (self.FORMA_PAGAMENT_ECO,        45),
                ),
            }
        }

    def get_opcions_quota(self, quota):
        return self._quotes().get(self.individual).get(quota)

    def get_opcions_quota_display(self, quota):

        opcions = OrderedDict()
        for forma_pagament, quantitat in self.get_opcions_quota(quota):
            forma_pagament_display = [d for f, d in self.FORMES_PAGAMENT_QUOTA_ALTA if f == forma_pagament][0]
            opcions[forma_pagament] = u"%0.2f %s" % (quantitat, forma_pagament_display)

        return opcions

    def get_quota(self, quota, forma_pagament):
        return dict(self.get_opcions_quota(quota)).get(forma_pagament)

    def actualitza_quotes(self):

        self.quota_alta_quantitat = self.get_quota(self.QUOTA_ALTA,
                                                   self.quota_alta_forma_pagament)

        self.quota_avancada_quantitat = self.get_quota(self.QUOTA_TRIMESTRAL_AVANCADA,
                                                       self.quota_avancada_forma_pagament)

    def get_nom_pas(self, n):
        return self.PASSOS[n].nom

    def get_nom_pas_actual(self):
        return self.PASSOS[self.pas]['nom']

    def es_firaire(self):
        return self.tipus == self.TIPUS_AUTOOCUPAT_FIRAIRE

    def es_inclus_lleugerament_firaire(self):
        if self.tipus == self.TIPUS_AUTOOCUPAT_FIRAIRE:
            return True
        if self.pk and self.altres_activitats.filter(firaire__isnull=False):
            return True

    @transaction.atomic
    def enregistra_canvi_i_salta_i_desa(self, user, pas_actual, pas_destinacio):
        """
        Guarda un registre de qui efectua canvis al procés i actualitza el pas en el qual queda el procés.
        El pas d'origen s'enten que és el que diu l'atribut self.pas, que no s'ha de modificar sino mitjançant
        aquest mètode.
        :param user: auth.User que està executant el canvi
        :param pas_actual: Pas que s'està executant (desant) i.e. pas del qual es fa un POST
        :param pas_destinacio: Pas al qual l'usuari vol anar (clica botó 'desar', 'desar i següent','desar i anterior'?
        :return: None
        """

        self.pas = pas_destinacio
        self.pas_maxim_assolit = max(self.pas, self.pas_maxim_assolit)
        self.responsable = user
        self.save()

        RegistreCanviProcesAltaAutoocupat(
            proces=self,
            pas_origen=self.pas,
            pas_actual=pas_actual,
            pas_destinacio=pas_destinacio,
            user=user
        ).save()


class RegistreCanviProcesAltaAutoocupat(models.Model):
    proces = models.ForeignKey(ProcesAltaAutoocupat)
    pas_origen = models.SmallIntegerField()
    pas_actual = models.SmallIntegerField()
    pas_destinacio = models.SmallIntegerField()
    user = models.ForeignKey(User)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class PeticioConcessioActivitat(models.Model):

    adreca = models.ForeignKey(AdrecaProjecteAutoocupat,
                               blank=True, null=True,
                               related_name='peticions_concessio_activitat')
    activitat = models.ForeignKey(Activitat)
    proces_alta_autoocupat = models.ForeignKey(ProcesAltaAutoocupat, related_name='peticions_concessio_activitat')
    comentaris_avaluacio = models.TextField(blank=True, null=True)

    numero_llicencia = models.CharField(
        blank=True, null=True,
        max_length=64,
        verbose_name=u"número de llicència",
        help_text=u"Al document de llicència que dona l'Ajuntament")

    data_inici_llicencia = models.DateField(
        blank=True, null=True,
        verbose_name=u"data d'inici del document original",
        help_text=u"Data d'inici del contracte amb l'Ajuntament")

    data_final_llicencia = models.DateField(
        blank=True, null=True,
        verbose_name=u"data final del document original",
        help_text=u"Data d'acabament del contracte amb l'Ajuntament")

    data_inici_concessio = models.DateField(
        blank=True, null=True,
        verbose_name=u"data d'inici de la concessió a la CIC",
        help_text=u"Data d'inici del contracte que signa el projecte amb la CIC")

    data_final_concessio = models.DateField(
        blank=True, null=True,
        verbose_name=u"data final de la concessió a la CIC",
        help_text=u"Data d'acabament del contracte que signa el projecte amb la CIC")

    comentaris_alta = models.TextField(blank=True, null=True)


class PeticioAsseguranca(models.Model):

    adreca = models.ForeignKey(AdrecaProjecteAutoocupat, blank=True, null=True)
    activitat = models.ForeignKey(Activitat, blank=True, null=True)
    proces_alta_autoocupat = models.ForeignKey(ProcesAltaAutoocupat, related_name='peticions_asseguranca')
    comentaris_avaluacio = models.TextField(blank=True, null=True)

    companyia_asseguradora = models.ForeignKey(EmpresaAsseguradora, blank=True, null=True)
    numero_polissa = models.CharField(max_length=128, blank=True, null=True, verbose_name=u"número de pòlissa")
    data_inici_polissa = models.DateField(blank=True, null=True, verbose_name=u"data d'inici de la cobertura")
    data_final_polissa = models.DateField(blank=True, null=True, verbose_name=u"data d'acabament de la cobertura")
    import_polissa = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True,
                                         verbose_name=u"import total periode cobertura")
    comentaris_alta = models.TextField(blank=True, null=True)


class FotoProjecteAutoocupatFiraire(models.Model):

    projecte = models.ForeignKey(ProcesAltaAutoocupat, related_name='fotos_parada_firaire')
    nom = models.CharField(max_length=256)
    detalls = models.TextField(blank=True, null=True)
    imatge = models.ImageField(upload_to='fotos_projecte_autoocupat_firaire')
    url_miniatura = models.CharField(max_length=1024, blank=True, null=True)


class ProjecteCollectiu(models.Model):

    TIPUS_SERVEIS = "de_serveis"
    TIPUS_CONSUM = "grup_de_consum"
    TIPUS_COLLECTIU = "cooperatiu_collectiu"
    TIPUS_PAIC = "paic_sense_facturacio"
    TIPUS = (
        (TIPUS_COLLECTIU, u"Projecte cooperatiu col·lectiu"),
        (TIPUS_SERVEIS, u"Projecte de serveis"),
        (TIPUS_CONSUM, u"Grup de consum"),
        (TIPUS_PAIC, u"PAIC sense facturació"),
        )

    FORMA_PAGAMENT_EURO = 'euro'
    FORMA_PAGAMENT_ECO = 'eco'
    FORMA_PAGAMENT_HORA = 'h'

    FORMES_PAGAMENT_QUOTA_ALTA = (
        (FORMA_PAGAMENT_ECO, u"ecos"),
        (FORMA_PAGAMENT_EURO, u"euros"),
        (FORMA_PAGAMENT_HORA, u"hores"),
    )

    QUOTA_ALTA = (
        ("euro", "60"),
        ("eco", "60"),
        ("h", "24"),
        )
    
    nom = models.CharField(max_length=64, unique=True)
    website = models.URLField(verbose_name=u"lloc web", null=True, blank=True)
    descripcio = models.TextField(verbose_name=u"descripció")
    email = models.EmailField()
    telefon = models.CharField(max_length=24, blank=True, null=True)
    membre_de_referencia = models.ForeignKey(Persona, verbose_name=u"membre de referencia")
    adreca = models.ForeignKey(AdrecaProjecteCollectiu, null=True)
    tipus_projecte = models.CharField(max_length=64, choices=TIPUS, default=TIPUS_COLLECTIU)
    quota_alta_forma_pagament = models.CharField(max_length=6,
                                                 choices=FORMES_PAGAMENT_QUOTA_ALTA,
                                                 default=FORMA_PAGAMENT_EURO)
    necessita_cobertura_legal = models.BooleanField(default=False)

    pagament_rebut = models.BooleanField(default=False)

    compte_CES_assignat = models.CharField(max_length=16, null=True)

    es_validat = models.BooleanField(default=False)
    dada_validacio = models.DateTimeField(null=True)
    usuaria_que_valida = models.ForeignKey(User, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = u"Projecte col·lectiu"
        verbose_name_plural = u"Projectes col·lectius"


class SociaCooperativa(models.Model):

    FORMA_PAGAMENT_EURO = 'euro'
    FORMA_PAGAMENT_ECO = 'eco'
    FORMA_PAGAMENT_HORA = 'h'

    FORMES_PAGAMENT_QUOTA_ALTA = (
        (FORMA_PAGAMENT_ECO, u"ecos"),
        (FORMA_PAGAMENT_EURO, u"euros"),
        (FORMA_PAGAMENT_HORA, u"hores"),
    )

    QUOTA_ALTA = (
        ("euro", "30"),
        ("eco", "30"),
        ("h", "12"),
    )

    socia = models.OneToOneField(Persona, unique=True)

    quota_alta_forma_pagament = models.CharField(max_length=6,
                                                 choices=FORMES_PAGAMENT_QUOTA_ALTA,
                                                 default=FORMA_PAGAMENT_EURO)
    quota_alta = models.CharField(max_length=12, choices=QUOTA_ALTA, default="euro")
    pagament_rebut = models.BooleanField(default=False)

    compte_CES_assignat = models.CharField(max_length=16, null=True)

    es_validada = models.BooleanField(default=False)
    dada_validacio = models.DateTimeField(null=True)
    usuaria_que_valida = models.ForeignKey(User, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = u"Socia Cooperativa"
        verbose_name_plural = u"Socies Cooperatives"


class SociaAfi(models.Model):

    TIPUS_DOCUMENT_ID_DNI = 'dni'
    TIPUS_DOCUMENT_ID_NIE = 'nie'
    TIPUS_DOCUMENT_ID_PASSAPORT = 'passaport'

    TIPUS_DOCUMENT_ID = (
        (TIPUS_DOCUMENT_ID_DNI, "dni"),
        (TIPUS_DOCUMENT_ID_NIE, "nie"),
        (TIPUS_DOCUMENT_ID_PASSAPORT, "passaport"),
    )

    nom = models.CharField(max_length=64, blank=True, null=True)
    cognom1 = models.CharField(max_length=64, blank=True, null=True)
    cognom2 = models.CharField(max_length=64, blank=True, null=True)
    pseudonim = models.CharField(max_length=64, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    telefon = models.CharField(max_length=24, blank=True, null=True)

    tipus_dni = models.CharField(max_length=6,
                                 choices=TIPUS_DOCUMENT_ID,
                                 default=TIPUS_DOCUMENT_ID_DNI)

    dni = models.CharField(max_length=16)

    usuaria_que_invita = models.ForeignKey(User, null=True, related_name='socia_afin_pendent_invitada')

    created_at = models.DateTimeField(auto_now_add=True)

    esta_pendent_de_validacio = models.BooleanField(default=True)
    
    validated_at = models.DateTimeField(null=True)
    usuaria_que_valida = models.ForeignKey(User, null=True, related_name='socia_afin_pendent_validada')

    projecte = models.ForeignKey(ProjecteCollectiu)

    class Meta:
        verbose_name = u"Socia Afí"
        verbose_name_plural = u"Socies Afins"
