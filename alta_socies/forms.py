# coding=utf-8
from datetime import timedelta

from django import forms
from django.core.exceptions import ValidationError
from django.contrib.auth.models import Group

from alta_socies.helpers import responsables_projecte_autoocupat

from .models import Ubicacio, ProcesAltaAutoocupat, \
    PeticioConcessioActivitat, PeticioAsseguranca, FotoProjecteAutoocupatFiraire
from socies.helpers import es_dni_valid
from socies.models import Activitat, AdrecaProjecteAutoocupat, ContribucioCIC, GrupHabilitat, Habilitat, Persona


class HabilitatsContribucionsModelMultipleChoiceField(forms.ModelMultipleChoiceField):
    """
    Custom field that renders Habilitats
    multiple choices
    """
    def label_from_instance(self, obj):
        return "%i - %s" % (obj.id, obj.nom)


class FormulariDadesGeneralsProjecteCollectiu(forms.Form):

    nom = forms.CharField(max_length=64, label=u"Nom del projecte")
    website = forms.URLField(required=False, label=u"Lloc web")
    descripcio = forms.CharField(max_length=2048, widget=forms.Textarea,
                                 label="Descripció")
    email = forms.EmailField(label=u"Correu electronic")
    telefon = forms.CharField(max_length=24, required=False, label=u"Telefon")

    pais = forms.CharField(max_length=128, label=u"País")
    provincia = forms.CharField(max_length=128, label=u"Provincia")
    comarca = forms.CharField(max_length=128, label=u"Comarca")
    poblacio = forms.CharField(max_length=128, label=u"Població")
    codi_postal = forms.CharField(max_length=16, label=u"Codi Postal")
    adreca = forms.CharField(max_length=128, label=u"Adreça")
    

class FormulariDadesConcretesProjecteCollectiu(forms.Form):

    TIPUS_SERVEIS = "de_serveis"
    TIPUS_CONSUM = "grup_de_consum"
    TIPUS_COLLECTIU = "cooperatiu_collectiu"
    TIPUS_PAIC = "paic_sense_facturacio"
    TIPUS = (
        (TIPUS_COLLECTIU, u"Projecte cooperatiu col·lectiu"),
        (TIPUS_SERVEIS, u"Projecte de serveis"),
        (TIPUS_CONSUM, u"Grup de consum"),
        (TIPUS_PAIC, u"PAIC sense facturació"),
    )

    FORMA_PAGAMENT_EURO = 'euro'
    FORMA_PAGAMENT_ECO = 'eco'
    FORMA_PAGAMENT_HORA = 'h'
    
    FORMES_PAGAMENT_QUOTA_ALTA = (
        (FORMA_PAGAMENT_ECO, u"60 ecos"),
        (FORMA_PAGAMENT_EURO, u"60 euros"),
        (FORMA_PAGAMENT_HORA, u"24 hores"),
    )
    
    membre_de_referencia = forms.ModelChoiceField(queryset=Persona.objects.all())
    tipus_projecte = forms.ChoiceField(choices=TIPUS)
    formes_pagament_quota_alta = forms.ChoiceField(choices=FORMES_PAGAMENT_QUOTA_ALTA)
    necessita_cobertura_legal = forms.BooleanField()


class FormulariResumSociaCooperativa(forms.Form):

    nom = forms.CharField(max_length=64, required=True, label="Nom")
    cognom1 = forms.CharField(max_length=64, required=False, label="Primer Cognom")
    cognom2 = forms.CharField(max_length=64, required=False, label="Segon Cognom")
    pseudonim = forms.CharField(max_length=64, required=False, label="Pseudónim")
    email = forms.EmailField(required=True, label="Correu elecronic")
    telefon = forms.CharField(max_length=64, required=False, label="Telefon")
    document = forms.CharField(max_length=64, required=False, label="Document")
    tipus_document = forms.CharField(max_length=16, required=False, label="Tipus Document")
    compte_integral_ces = forms.CharField(max_length=12, required=False, label="Compte IntegralCES")
    cuota_alta = forms.CharField(max_length=12, required=True)
    contribucions = forms.CharField(max_length=1024, required=False,
                                    label=u"Tipus de contribució a la CIC")
    habilitats = forms.CharField(required=False, max_length=1024)
    detalls_habilitats = forms.CharField(max_length=1024, required=False, widget=forms.Textarea,
                                         label=u"Detalls sobre habilitats, coneixements i experiència")
 

class FormulariDadesAltaSociaCooperativa(forms.Form):

    QUOTA_ALTA = (
        ("euro", "30 euros"),
        ("eco", "30 ecos"),
        ("h", "12 horas"),
    )
    cuota_alta = forms.ChoiceField(required=True, choices=QUOTA_ALTA)
    te_compte_integral_ces = forms.BooleanField(required=False, widget=forms.CheckboxInput(attrs={
                                               'style': 'display:none'
                                               }))
    contribucions = HabilitatsContribucionsModelMultipleChoiceField(required=False,
                                                                   widget=forms.CheckboxSelectMultiple,
                                                                   label=u"Tipus de contribució a la CIC",
                                                                   queryset=ContribucioCIC.objects.all())
    habilitats = HabilitatsContribucionsModelMultipleChoiceField(required=False,
                                                    queryset=Habilitat.objects.all())
    detalls_habilitats = forms.CharField(max_length=1024, required=False, widget=forms.Textarea,
                                         label=u"Detalls sobre habilitats, coneixements i experiència")
    
    def clean(self):
        cleaned_data = super(FormulariDadesAltaSociaCooperativa, self).clean()
        cuota_alta = cleaned_data.get('cuota_alta')
        tiene_cuenta_ces = cleaned_data.get('te_compte_integral_ces')
        if cuota_alta == 'eco' and not tiene_cuenta_ces:
            raise forms.ValidationError(
                "No pots pagar amb ecos si no tens un compte a l'IntegralCES"
            )

    
class FormulariDadesPersonalsSociaCooperativa(forms.Form):

    TIPUS_DOCUMENT_ID_DNI = 'dni'
    TIPUS_DOCUMENT_ID_NIE = 'nie'
    TIPUS_DOCUMENT_ID_PASSAPORT = 'passaport'
    
    TIPUS_DOCUMENT_ID = (
        (TIPUS_DOCUMENT_ID_DNI, "dni"),
        (TIPUS_DOCUMENT_ID_NIE, "nie"),
        (TIPUS_DOCUMENT_ID_PASSAPORT, "passaport"),
    )

    nom = forms.CharField(max_length=64, label="Nom")
    cognom1 = forms.CharField(max_length=64, required=False, label="Primer Cognom")
    cognom2 = forms.CharField(max_length=64, required=False, label="Segon Cognom")
    pseudonim = forms.CharField(max_length=64, required=False, label="Pseudónim")
    email = forms.EmailField(required=True, label="Correu elecronic")
    telefon = forms.CharField(max_length=64, required=False, label="Telefon")
    document = forms.CharField(max_length=64, required=False, label="Document")
    tipus_document = forms.ChoiceField(label="Tipus Document", required=False, choices=TIPUS_DOCUMENT_ID)
    te_compte_integral_ces = forms.BooleanField(label="Tinc compte IntegralCES", required=False,
                                                widget=forms.CheckboxInput(attrs={
                                                    'onclick': "javascript:toggleDiv('compte_ces');"
                                                }))
    compte_integral_ces = forms.CharField(max_length=12, required=False, label="Compte IntegralCES",
                                          widget=forms.TextInput(attrs={
                                              'id': 'compte_ces',
                                              'style': 'display:none',
                                          }))


class FormulariRebutjarProcesAltaAutoocupat(forms.Form):

    motiu = forms.CharField(max_length=256, widget=forms.Textarea)
    confirmacio = forms.BooleanField(label=u"Confirmo que aquest procés d'alta s'abandona en aquest punt")


class FormulariCercaProcesAltaAutoocupat(forms.Form):

    per_responsable = forms.ChoiceField(required=False, label=u"Per responsable d'alta")
    per_nom = forms.CharField(required=False, max_length=64, label=u"Per nom del projecte")
    per_email = forms.CharField(required=False, max_length=64, label=u"Per email del projecte")
    per_estat = forms.ChoiceField(required=False, label=u"Per estat")

    def __init__(self, *args, **kwargs):

        super(FormulariCercaProcesAltaAutoocupat, self).__init__(*args, **kwargs)

        responsables = responsables_projecte_autoocupat()
        responsables = responsables.order_by('first_name', 'last_name')
        responsables = responsables.values_list('id', 'first_name', 'last_name')

        responsables_choices = [(0, u"(qualsevol)")]
        for pk, f, l in responsables:
            responsables_choices.append((pk, ' '.join([f, l]),))

        per_estat_choices = [('*', u"(qualsevol)")]
        per_estat_choices += list(ProcesAltaAutoocupat.RESOLUCIONS)

        self.fields['per_estat'].choices = per_estat_choices
        self.fields['per_responsable'].choices = responsables_choices


class FormulariReassignarProcesAltaAutoocupat(forms.Form):

    destinatari = forms.ChoiceField(required=True)

    def __init__(self, *args, **kwargs):

        destinataris = kwargs.pop('destinataris')
        destinataris = destinataris.order_by('first_name', 'last_name')
        destinataris = destinataris.values_list('id', 'first_name', 'last_name')
        destinataris_choices = []
        for pk, f, l in destinataris:
            destinataris_choices.append((pk, ' '.join([f,l]),))

        super(FormulariReassignarProcesAltaAutoocupat, self).__init__(*args, **kwargs)

        self.fields['destinatari'].choices = destinataris_choices


class FormulariAssistenciaAcollida(forms.ModelForm):

    class Meta:
        model = ProcesAltaAutoocupat
        fields = ['nom', 'email', 'telefon', ]


class FormulariAltaProjecteAutoocupatSessioMoneda(forms.ModelForm):

    class Meta:
        model = ProcesAltaAutoocupat
        fields = ['sessio_moneda']

    def __init__(self, *args, **kwargs):
        super(FormulariAltaProjecteAutoocupatSessioMoneda, self).__init__(*args, **kwargs)

        self.fields['sessio_moneda'].required = True


class FormulariAltaProjecteAutoocupatCessioUs(forms.ModelForm):

    class Meta:
        model = AdrecaProjecteAutoocupat
        fields = ['traspas_inici', 'traspas_final', 'traspas_comentaris_alta']

    def __init__(self, *args, **kwargs):
        super(FormulariAltaProjecteAutoocupatCessioUs, self).__init__(*args, **kwargs)

        self.fields['traspas_inici'].required = True
        self.fields['traspas_final'].required = True


class FormulariAltaProjecteAutoocupatLloguer(forms.ModelForm):

    class Meta:
        model = AdrecaProjecteAutoocupat
        fields = ['traspas_inici', 'traspas_final', 'traspas_comentaris_alta',
                  'traspas_lloguer_import_mensual']

    def __init__(self, *args, **kwargs):
        super(FormulariAltaProjecteAutoocupatLloguer, self).__init__(*args, **kwargs)

        self.fields['traspas_inici'].required = True
        self.fields['traspas_final'].required = True
        self.fields['traspas_lloguer_import_mensual'].required = True


# TODO aquest nom s'hauria de canviar; ja ho avaluare quan fem les altes "d'altres socis"
class FormulariProjecte(forms.ModelForm):

    class Meta:
        model = ProcesAltaAutoocupat
        fields = ['sessio_acollida', 'nom', 'email', 'telefon', ]

    def clean(self):

        cleaned_data = super(FormulariProjecte, self).clean()

        nom = cleaned_data.get('nom')
        if nom:
            cleaned_data['nom'] = nom.strip()
        # TODO clean also email and telefon?

        if ProcesAltaAutoocupat.objects.filter(nom__iexact=nom).exclude(pk=self.instance.pk).exists():
            self.add_error('nom', "Ja hi ha un projecte amb aquest nom!")

        return cleaned_data


# TODO aquest nom s'hauria de canviar; ja ho avaluare quan fem les altes "d'altres socis"
class FormulariDadesGeneralsProjecte(forms.ModelForm):

    class Meta:
        model = ProcesAltaAutoocupat
        fields = ['descripcio',
                  'website',
                  'tipus',
                  'individual',
                  'te_comerc_electronic',
                  'te_productes_ecologics',
                  'tipus_parada_firaire',
                  'expositors']

    def __init__(self, *args, **kwargs):
        super(FormulariDadesGeneralsProjecte, self).__init__(*args, **kwargs)

        self.fields['tipus'].required = True
        self.fields['tipus'].widget = forms.RadioSelect(choices=ProcesAltaAutoocupat.TIPUS)

        self.fields['individual'].required = True
        self.fields['individual'].widget = forms.RadioSelect(choices=ProcesAltaAutoocupat.INDIVIDUAL)

        self.fields['descripcio'].widget = forms.Textarea(attrs={'class': 'descripcio_field',
                                                                 'rows': 15})
        self.fields['website'].widget = forms.URLInput(attrs={'class': 'url_field'})
        self.fields['tipus_parada_firaire'].widget = forms.RadioSelect(choices=ProcesAltaAutoocupat.PARADA_FIRAIRE)

        self.fields['descripcio'].required = True


class FormulariDetallsMembreDeReferencia(forms.Form):

    # una mica cutre; s'ha de mantenir "sincronitzat" amb socies.Persona
    id = forms.IntegerField()
    nom_sencer = forms.CharField(max_length=360)
    email = forms.EmailField(label=u"Correu electrònic")
    tipus_document_dni = forms.CharField(max_length=16)
    dni = forms.CharField(max_length=64)
    compte_integralces = forms.CharField(max_length=16, required=False,
                                         label=u"IntegralCES", help_text=u"Número de compte a IntegralCES")
    contribucions = forms.MultipleChoiceField(required=False,
                                              widget=forms.CheckboxSelectMultiple,
                                              label=u"Tipus de contribució a la CIC")
    habilitats = forms.MultipleChoiceField(required=False, label=u"Habilitats, coneixements i experiència")
    detalls_habilitats = forms.CharField(max_length=1024, required=False,
                                         widget=forms.Textarea,
                                         label=u"Detalls sobre habilitats, coneixements i experiència")

    def __init__(self, *args, **kwargs):
        super(FormulariDetallsMembreDeReferencia, self).__init__(*args, **kwargs)
        self.fields['id'].widget = forms.HiddenInput()
        self.fields['nom_sencer'].widget = forms.HiddenInput()
        self.fields['tipus_document_dni'].widget = forms.Select(choices=Persona.TIPUS_DOCUMENT_ID)
        self.fields['contribucions'].choices = ContribucioCIC.objects.values_list('id', 'nom')
        self.fields['contribucions'].widget.attrs = {'size': min(len(self.fields['contribucions'].choices), 5)}
        self.fields['dni'].widget.attrs = {'data-dni': ''}

        habilitats_choices = []
        for hg in GrupHabilitat.objects.all():
            habilitats_choices.append(
                (
                    hg.nom,
                    tuple(hg.habilitat_set.values_list('id', 'nom'))
                )
            )
        self.fields['habilitats'].choices = habilitats_choices
        self.fields['habilitats'].widget.attrs = {'size': 15}
        self.fields['detalls_habilitats'].widget.attrs = {'class': 'detalls_habilitats'}

    def clean(self):

        cleaned_data = super(FormulariDetallsMembreDeReferencia, self).clean()

        dni = cleaned_data.get('dni')
        if dni:
            dni = dni.strip()
            cleaned_data['dni'] = dni

            tipus = cleaned_data.get('tipus_document_dni')
            if tipus == Persona.TIPUS_DOCUMENT_ID_DNI:
                if not es_dni_valid(dni):
                    self.add_error('dni', u"Això no és un DNI vàlid")

        return cleaned_data


# TODO aquest formulari potser hauria de dir-se FormulariGraellaPersona, i derivar per herència un amb el nom actual
class FormulariMembreDeReferencia(forms.Form):
    """
    Hem de mantenir aquesta llista de camps i propietats
    sincronitzada amb la definicio de Persona!
    """

    _extra_attributes = {'onchange': 'camp_persona_modificat(event.target);',
                         'onfocus':  'canviar_de_fila(event.target);',
                         'onblur':   'canviar_de_fila(event.target);'}

    id = forms.IntegerField(required=False, widget=forms.HiddenInput)
    nom = forms.CharField(max_length=64, required=False, widget=forms.TextInput(attrs=_extra_attributes))
    cognom1 = forms.CharField(max_length=64, required=False, widget=forms.TextInput(attrs=_extra_attributes))
    cognom2 = forms.CharField(max_length=64, required=False, widget=forms.TextInput(attrs=_extra_attributes))
    # pseudonim = forms.CharField(max_length=64, required=False, widget=forms.TextInput(attrs=_extra_attributes))
    # email = forms.EmailField(required=False, widget=forms.TextInput(attrs=_extra_attributes))
    telefon = forms.CharField(max_length=64, required=False, widget=forms.TextInput(attrs=_extra_attributes))


class FormulariAdrecaProjecteAutoocupat(forms.ModelForm):

    class Meta:
        model = AdrecaProjecteAutoocupat
        fields = '__all__'
        widgets = {
            'traspas': forms.RadioSelect(),
            'activitats': forms.CheckboxSelectMultiple()
        }


class FormulariCercaActualitzacioSeleccioPersones(forms.ModelForm):

    id_persona = forms.IntegerField(required=False)
    persones_escollides = forms.CharField(max_length=1024, required=False)

    class Meta:
        model = Persona
        fields = ['nom', 'cognom1', 'cognom2', 'pseudonim', 'email',
                  'telefon', 'compte_integralces', 'tipus_document_dni', 'dni']

    def __init__(self, *args, **kwargs):

        # TODO aquesta merda dels camps obligatoris s'ha de refer amb herència del formulari
        camps_obligatoris = []
        if 'camps_obligatoris' in kwargs.keys():
            camps_obligatoris = kwargs.get('camps_obligatoris')
            del(kwargs['camps_obligatoris'])

        super(FormulariCercaActualitzacioSeleccioPersones, self).__init__(*args, **kwargs)

        self.fields['dni'].widget.attrs = {'autocomplete': 'off',
                                           'data-dni': ''}

        self.fields['id_persona'].widget = forms.HiddenInput()
        self.fields['persones_escollides'].widget = forms.HiddenInput()

        for camp in camps_obligatoris:
            assert(camp in self.Meta.fields)
            self.fields[camp].required = True

        for camp in self.Meta.fields:
            if camp != 'dni':
                self.fields[camp].widget.attrs = {'onkeydown': 'return capturar_tecla_enter(event);',
                                                  'autocomplete': 'off'}


class ForulariPas19(forms.ModelForm):

    class Meta:
        model = ProcesAltaAutoocupat
        fields = ['compte_CES_assignat']

    def __init__(self, *args, **kwargs):

        super(ForulariPas19, self).__init__(*args, **kwargs)

        self.fields['compte_CES_assignat'].required = True


class FormulariFotoProjecteAutoocupatFiraire(forms.ModelForm):

    imatges_a_eliminar = forms.CharField(max_length=1024, required=False, widget=forms.HiddenInput)

    class Meta:
        model = FotoProjecteAutoocupatFiraire
        fields = ['nom', 'detalls', 'imatge']

    def __init__(self, *args, **kwargs):
        super(FormulariFotoProjecteAutoocupatFiraire, self).__init__(*args, **kwargs)
        self.fields['detalls'].widget = forms.Textarea(attrs={'rows': 3})


class FormulariAltresActivitatsProjecteAutoocupatFiraire(forms.ModelForm):

    class Meta:
        model = ProcesAltaAutoocupat
        fields = ['altres_activitats']
        widgets = {'altres_activitats': forms.CheckboxSelectMultiple()}


class FormulariAltresActivitatsProjecteAutoocupatNoFiraire(FormulariAltresActivitatsProjecteAutoocupatFiraire):

    def __init__(self, *args, **kwargs):
        super(FormulariAltresActivitatsProjecteAutoocupatNoFiraire, self).__init__(*args, **kwargs)
        self.fields['altres_activitats'].queryset = Activitat.objects.filter(firaire=False)


class FormulariFormaPagamentQuotesAutoocupat(forms.ModelForm):

    class Meta:
        model = ProcesAltaAutoocupat
        fields = ['quota_alta_forma_pagament', 'quota_avancada_forma_pagament']

    def __init__(self, *args, **kwargs):
        super(FormulariFormaPagamentQuotesAutoocupat, self).__init__(*args, **kwargs)

        proces = kwargs.get('instance')

        opcions = proces.get_opcions_quota_display(proces.QUOTA_ALTA)
        opcions = ((k, v) for k, v in opcions.iteritems())
        self.fields['quota_alta_forma_pagament'].widget = forms.RadioSelect(choices=opcions)

        opcions = proces.get_opcions_quota_display(proces.QUOTA_TRIMESTRAL_AVANCADA)
        opcions = ((k, v) for k, v in opcions.iteritems())
        self.fields['quota_avancada_forma_pagament'].widget = forms.RadioSelect(choices=opcions)


class FormulariAltaConcessioActivitat(forms.ModelForm):

    class Meta:
        model = PeticioConcessioActivitat
        fields = ['numero_llicencia',
                  'data_inici_llicencia',
                  'data_final_llicencia',
                  'data_inici_concessio',
                  'data_final_concessio',
                  'comentaris_alta']

    def __init__(self, *args, **kwargs):
        super(FormulariAltaConcessioActivitat, self).__init__(*args, **kwargs)

        self.fields['numero_llicencia'].required = True
        self.fields['data_inici_llicencia'].required = True
        self.fields['data_final_llicencia'].required = False
        self.fields['data_inici_concessio'].required = True
        self.fields['data_final_concessio'].required = False
        self.fields['comentaris_alta'].widget = forms.Textarea(attrs={'rows': 3})


class FormulariAltaAsseguranca(forms.ModelForm):

    class Meta:
        model = PeticioAsseguranca
        fields = ['companyia_asseguradora',
                  'numero_polissa',
                  'data_inici_polissa',
                  'data_final_polissa',
                  'import_polissa',
                  'comentaris_alta']

    def __init__(self, *args, **kwargs):
        super(FormulariAltaAsseguranca, self).__init__(*args, **kwargs)

        self.fields['companyia_asseguradora'].required = True
        self.fields['numero_polissa'].required = True
        self.fields['data_inici_polissa'].required = True
        self.fields['data_final_polissa'].required = True
        self.fields['import_polissa'].required = True
        self.fields['comentaris_alta'].widget = forms.Textarea(attrs={'rows': 3})


class FormulariAltaQuotes(forms.ModelForm):

    alta_confirmacio = forms.BooleanField(widget=forms.CheckboxInput,
                                          initial=False,
                                          label=u"He rebut el pagament")

    avancada_confirmacio = forms.BooleanField(widget=forms.CheckboxInput,
                                              initial=False,
                                              label=u"He rebut el pagament")

    class Meta:
        model = ProcesAltaAutoocupat
        fields = ['quota_alta_forma_pagament',
                  'quota_avancada_forma_pagament',
                  'compte_CES_que_paga_les_quotes',
                  # 'pagament_quotes_numero_transaccio',
                  'comentaris_pagament_quotes']

    def __init__(self, *args, **kwargs):
        super(FormulariAltaQuotes, self).__init__(*args, **kwargs)

        proces = kwargs.get('instance')

        self.fields['comentaris_pagament_quotes'].widget = forms.Textarea(attrs={'rows': 3})

        opcions = proces.get_opcions_quota_display(proces.QUOTA_ALTA)
        if proces.quota_alta_forma_pagament != proces.FORMA_PAGAMENT_HORA:
            del opcions[proces.FORMA_PAGAMENT_HORA]
        opcions = ((k, v) for k, v in opcions.iteritems())
        self.fields['quota_alta_forma_pagament'].widget = forms.RadioSelect(choices=opcions)

        opcions = proces.get_opcions_quota_display(proces.QUOTA_TRIMESTRAL_AVANCADA)
        opcions = ((k, v) for k, v in opcions.iteritems())
        self.fields['quota_avancada_forma_pagament'].widget = forms.RadioSelect(choices=opcions)

        self.fields['quota_alta_forma_pagament'].label = u"Quota d'alta"
        self.fields['quota_avancada_forma_pagament'].label = u"Quota trimestral avançada"
        self.fields['compte_CES_que_paga_les_quotes'].label = u"Compte de l'IntegralCES que paga les quotes"
        # self.fields['pagament_quotes_numero_transaccio'].label = u"Número de transacció a l'IntegralCES"

    def clean(self):

        cleaned_data = super(FormulariAltaQuotes, self).clean()

        quelcom_es_paga_en_ecos = (
            cleaned_data.get('quota_alta_forma_pagament') == ProcesAltaAutoocupat.FORMA_PAGAMENT_ECO or
            cleaned_data.get('quota_avancada_forma_pagament') == ProcesAltaAutoocupat.FORMA_PAGAMENT_ECO)

        cleaned_data['compte_CES_que_paga_les_quotes'] = cleaned_data.get('compte_CES_que_paga_les_quotes').upper()
        if quelcom_es_paga_en_ecos:
            if not cleaned_data.get('compte_CES_que_paga_les_quotes').startswith('COOP'):
                self.add_error('compte_CES_que_paga_les_quotes',
                               u"Camp obligatori si es paga alguna quota en ecos. Ha de començar per \"COOP\".")
            # per ara l'integralces no informa clarament de cap número de transacció. No ho fem obligatori, doncs.
            # if not cleaned_data.get('pagament_quotes_numero_transaccio'):
            #     self.add_error('pagament_quotes_numero_transaccio',
            #                    u"Copieu aquí el número de transacció que ha donat l'IntegralCES.")

        if not (cleaned_data.get('alta_confirmacio', False) and cleaned_data.get('avancada_confirmacio', False)):
            self.add_error('__all__', u"S'han de fer efectives les dues quotes!")

        return cleaned_data


class GeneradorSessions(forms.Form):

    # TODO refactor treure els tipus de sessió d'aqui :)
    # TODO keep in sync with alta_socies.views.sessions_del_mes
    TIPUS_SESSIO_AVALUACIO = 'avaluacio'
    TIPUS_SESSIO_ALTA = 'alta'
    TIPUS_DE_SESSIONS = (
        (TIPUS_SESSIO_AVALUACIO, "avaluació"),
        (TIPUS_SESSIO_ALTA, "alta"),
    )

    tipus_sessio = forms.ChoiceField(
        choices=TIPUS_DE_SESSIONS,
        widget=forms.RadioSelect)

    responsables_alta = forms.ModelMultipleChoiceField(
        queryset=None,
        widget=forms.CheckboxSelectMultiple)

    data_i_hora_inici = forms.DateTimeField(
        label="Data i hora d'inici de la primera sessió",
        widget=forms.SplitDateTimeWidget(time_format='%H:%M'))

    duracio = forms.IntegerField(
        label="duració de cada sessió",
        help_text="minuts",
        min_value=15,
        max_value=300)

    num_sessions = forms.IntegerField(
        label="Número de sessions",
        min_value=1,
        max_value=60)

    ubicacio = forms.ModelChoiceField(
        label="Ubicació",
        queryset=None)

    assabentada_del_risc = forms.BooleanField(
        label="Entenc el risc",
        help_text="ATENCIÓ! aquesta eina no comprova si les sessions que crearà "
                  "es solapen amb altres ja donades d'alta, ni té en compte la "
                  "disponibilitat dels responsables d'alta, espais o recursos necessaris!")

    def __init__(self, *args, **kwargs):
        super(GeneradorSessions, self).__init__(*args, **kwargs)

        responsables_alta = Group.objects.get(name='''Responsables d'alta''').user_set.all()
        ubicacions = Ubicacio.objects.all()

        self.fields['responsables_alta'].queryset = responsables_alta
        self.fields['ubicacio'].queryset = ubicacions

    def clean(self):

        cleaned_data = super(GeneradorSessions, self).clean()

        inici = cleaned_data.get('data_i_hora_inici')
        duracio = cleaned_data.get('duracio')
        num_sessions = cleaned_data.get('num_sessions')
        if inici and duracio and num_sessions:
            final = inici + timedelta(minutes=duracio * num_sessions, seconds=-1)
            if inici.day != final.day or inici.month != final.month or inici.year != final.year:
                raise ValidationError("L'última sessió ha d'acabar abans de mitjanit")
