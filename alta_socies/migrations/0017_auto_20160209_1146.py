# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alta_socies', '0016_remove_projectecollectiu_socies_afins_adicionals'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ubicacio',
            name='comarca',
            field=models.CharField(max_length=128, blank=True),
        ),
        migrations.AlterField(
            model_name='ubicacio',
            name='pais',
            field=models.CharField(max_length=128, verbose_name='pa\xeds', blank=True),
        ),
        migrations.AlterField(
            model_name='ubicacio',
            name='provincia',
            field=models.CharField(max_length=128, verbose_name='prov\xedncia', blank=True),
        ),
        migrations.AlterField(
            model_name='ubicacio',
            name='ubicacio_especifica',
            field=models.CharField(default='', max_length=256, verbose_name='ubicaci\xf3 espec\xedfica', blank=True),
            preserve_default=False,
        ),
    ]
