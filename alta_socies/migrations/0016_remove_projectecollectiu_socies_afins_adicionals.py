# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alta_socies', '0015_auto_20160106_0147'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='projectecollectiu',
            name='socies_afins_adicionals',
        ),
    ]
