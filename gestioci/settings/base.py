"""
Django settings for GestioCI project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

import os
# import sys # commented as unused
from PIL import Image
from django.core.exceptions import ImproperlyConfigured
from django.contrib.messages import constants as messages

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

# Hack: Insert apps path to Python path
# TODO: Use full paths on files -> "import gestioci.apps.alta_socies..."?
# TODO: @doup: apps is not a python module, for it to be you should "touch gestioci/apps/__init__.py"
# http://stackoverflow.com/questions/3948356/how-to-keep-all-my-django-applications-in-specific-folder
# sys.path.insert(0, os.path.join(BASE_DIR, 'gestioci/apps'))


def get_env_variable(var_name):
    """Get the environment variable or raise exception"""

    try:
        return os.environ[var_name]

    except KeyError:
        error_msg = "Please set the %s environment variable" % var_name
        raise ImproperlyConfigured(error_msg)


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
TEMPLATE_DEBUG = False

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = get_env_variable("GCI_SECRET_KEY")

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    # Django
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Vendors
    'django_cron',
    # Apps
    'inici',
    'alta_socies',
    'empreses',
    'socies',
    'projectes',
    'facturacio',
    # Django Admin
    # We put this in last position as we override
    # some admin default templates in our apps.
    'django.contrib.admin',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.gzip.GZipMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

############################
# Security by default
CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True
X_FRAME_OPTIONS = 'DENY'
############################

ROOT_URLCONF = 'gestioci.urls'
WSGI_APPLICATION = 'gestioci.wsgi.application'

LOGIN_URL = '/accounts/login/'
LOGIN_REDIRECT_URL = '/inici'


# Feature Flags
FEATURE_FLAGS = {
    'FLAG_ALTA_SOCIES': False,
    'FLAG_FAQ':         False,
    'FLAG_GECO':        False,
    'FLAG_LETTUCE':     False,
}


if FEATURE_FLAGS['FLAG_LETTUCE']:
    INSTALLED_APPS += ('lettuce.django',)
    LETTUCE_USE_TEST_DATABASE = True  # Use the test database for models


# https://docs.djangoproject.com/en/1.7/topics/i18n/timezones/
TIME_ZONE = 'Europe/Madrid'  # was 'UTC'
USE_TZ = True


# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/
LANGUAGE_CODE = 'ca'
USE_I18N = True

USE_L10N = False

SHORT_DATE_FORMAT = 'D j b Y'
DATE_FORMAT = 'D j b Y'
DATETIME_FORMAT = 'D j b Y H:i'
TIME_FORMAT = 'H:i'


TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'gestioci.context_processors.core',
    'django.contrib.messages.context_processors.messages',
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'gestioci/templates'),
)

# Message Framework settings
MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'
MESSAGE_TAGS = {
    messages.ERROR: 'alert',
}
# FIXTURE_DIRS = (
#     "gestioci/fixtures/",
# )


# Thumbnails

THUMBNAILS_SUBDIR = ".miniatures"
THUMBNAIL_MAX_SIZE = (240, 240)
THUMBNAIL_FILTER = Image.ANTIALIAS

# GestioCI

GCI_RELEASE = u'1.10.5'

# Mail server for auth tasks

EMAIL_USE_TLS = True
EMAIL_HOST = 'mail.cooperativa.cat'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'noresponc'
EMAIL_HOST_PASSWORD = get_env_variable("GCI_EMAIL_HOST_PASSWORD")
DEFAULT_FROM_EMAIL = 'noresponc@cooperativa.cat'

# Cron jobs

CRON_CLASSES = [
    "facturacio.cronjobs.CronJobMantenimentTrimestres",
    "facturacio.cronjobs.CronJobMantenimentSequenciaNumeroFacturaEmesa",
]
