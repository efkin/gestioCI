# coding=utf-8

# En aquest fitxer no s'ha de traduïr res;
# és per això que ens permetem no respectar la convenció de que
# els literals no traduïbles es delimiten amb single quote; ho
# fem per què d'aquesta manera es poden escriure els apòstrofs
# sense escapar :)

# TODO we should probably add a prefix to group names (eg. "autoocupats - Responsables d'alta")
RESPONSABLES_ALTA = "Responsables d'alta"
SUPERVISORES_ALTA = "Supervisores d'alta"
EXPORTADORES = "Exportadores"
RESPONSABLES_FACTURACIO = "Responsables de facturació"

# TODO figure a way to automatically fill this array with introspection
grups = [
    RESPONSABLES_ALTA,
    SUPERVISORES_ALTA,
    EXPORTADORES,
    RESPONSABLES_FACTURACIO,
]

