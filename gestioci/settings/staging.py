"""
Settings specific to the STAGING environment
These settings should match as much as posisble those on the PRODUCTION environment
"""

from .production import *

ENV = 'staging'

# SECURITY WARNING: don't run with debug turned on in production!
assert(DEBUG is False)
assert(TEMPLATE_DEBUG is False)


ALLOWED_HOSTS = [
    'gestio-stage.cooperativa.cat',
]

BASE_URL = 'https://gestio-stage.cooperativa.cat:7582'  # no trailing slash please
LOGIN_URL = '%s/accounts/login/' % BASE_URL
LOGIN_REDIRECT_URL = '%s/' % BASE_URL
LOGOUT_URL = LOGIN_URL
