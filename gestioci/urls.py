from django.conf import settings
from django.conf.urls import include, patterns, url
from django.contrib import admin
from django.contrib.auth import views as auth_views

urlpatterns = patterns(
    '',

    # Default Django Auth
    # https://docs.djangoproject.com/en/1.8/topics/auth/default/#built-in-auth-views
    url(r'^accounts/', include([
        url(r'^login/$', auth_views.login, name='login'),
        url(r'^logout/$', auth_views.logout, {'next_page': '/accounts/login/'}, name='logout'),

        url(r'^password/', include([
            url(r'^change/$', auth_views.password_change, name='password_change'),
            url(r'^change/done/$', auth_views.password_change_done, name='password_change_done'),

            url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', auth_views.password_reset_confirm, name='password_reset_confirm'),
            url(r'^reset/complete/$', auth_views.password_reset_complete, name='password_reset_complete'),
            url(r'^reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
            url(r'^reset/$', auth_views.password_reset, { 'html_email_template_name': 'registration/password_reset_email.html' }, name='password_reset'),
        ])),
    ])),


    # Apps
    url(r'^socies/alta/', include('alta_socies.urls', namespace='alta_socies')),
    url(r'^socies/',      include('socies.urls', namespace='socies')),
    url(r'^facturacio/',  include('facturacio.urls', namespace='facturacio')),

    url(r'^admin/',       include(admin.site.urls)),

    url(r'^inici/$',       'inici.views.inici',                      name='inici'),
    url(r'^ge/$',          'inici.views.dashboard_gestio_economica', name='dashboard_gestio_economica'),
    url(r'^pao/$',         'facturacio.views.dashboard_facturacio',  name='dashboard_projecte_autoocupat'),
    url(r'^sc/$',          'socies.views.dashboard_socia_cooperativa',           name='dashboard_socia_cooperativa'),
    url(r'^style-guide/$', 'inici.views.styleguide',                 name='styleguide'),

    url(r'^/?$',            'inici.views.index',                      name='index'),
)

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns(
        '',
        (
            r'^media/(?P<path>.*)$',
            'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT}
        ),
    )

# Implement docutils features
if settings.ENV == 'development':
    urlpatterns += patterns(
        '',
        ( r'^admin/doc/', include('django.contrib.admindocs.urls')),
        )

