from django.conf import settings


def core(request):
    return dict({
        'ENV': settings.ENV,
        'DEBUG': settings.DEBUG,
        'VERSION': settings.GCI_RELEASE,
    }, **settings.FEATURE_FLAGS)
