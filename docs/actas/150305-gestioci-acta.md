Orden del día 15-03-05
======================

Informativos
------------

* Update refactor (efkin)
he estado trabajando, y me queda entender el js del cercador de personas y esperar cambios de template a nuevo css para seguir con nuevos pasos

* Update CSS (asier)
he implementado node.js tras mirar diferentes opciones. he incluido bower como package manager, gulp como build-system, y foundation como css framework. he creado dos template tags.

* Update bugfixes (pablo)

* About git
  (pequeña charla sobre git en conjunto)

* Revisión calendario

* Posible critical security hole
(si el archivo settings.py en el branch producción está
tal cual en el server de producción tenemos un problema
gordo aunque tiene facil solución)
   [descartado pero destapada configuración impropria]

Propuestas
----------

* Mantener las actas en el redmine
(estaría bien mantener las actas en el redmine, ya sea en el repo que en la wiki)

* Key-signing party [DONE]
(para poder verificar los commits y poder autenticar los mensajes por lista estaría bien firmarnos las claves)

* Compartir "palmera" del lidl [DONE]

Tareas
------

* Elaborar documento 'protocolo a producción'[efkin]
* Dividir archivo settings.py [pablo]
* Configurar nginx y wsgi.py [pablo y verneda]
* Seguir con CSS [asier] [DONE]
* Seguir refactor [efkin]
* Acabar bugfixes [pablo]
* pre-commit hook pep8 [efkin] [DONE]
* Subir actas al redmine [asier]
