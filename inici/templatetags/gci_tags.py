# coding=utf-8
from django import template
from django.contrib.auth.models import User
from django.forms import fields
from gestioci import formats
from gestioci.settings import auth_groups

register = template.Library()


@register.inclusion_tag('tags/form_field.html')
def form_field(field, hide_label=False, hide_help=False):
    field.field.widget.attrs.update({"data-test-field": field.label})

    return {
        'field': field,
        'hide_label': hide_label,
        'hide_help': hide_help,
    }


@register.inclusion_tag('tags/currency.html')
def currency_value(amount, highlight_negative=False, currency_symbol=u"€"):

    formatted_value = '{:,}'.format(amount).replace(',', ' ')  # .replace('.', ',').replace(' ', '.')

    negative = False
    if highlight_negative:
        negative = amount < 0

    return {
        'formatted_value': formatted_value,
        'negative': negative,
        'currency_symbol': currency_symbol,
    }


@register.filter(name="format_data_i_hora")
def format_data_i_hora(datetime):
    return formats.format_data_i_hora(datetime)


@register.filter(name="format_data")
def format_data(date):
    return formats.format_data(date)


@register.filter(name="format_data_mes_i_any")
def format_data_mes_i_any(date):
    return formats.format_data_mes_i_any(date)


@register.filter(name="format_data_dow_dom")
def format_data_dow_dom(date):
    return formats.format_data_dow_dom(date)


@register.filter(name="format_hora_hh_mm")
def format_hora_hh_mm(datetime):
    return formats.format_hora_hh_mm(datetime)


@register.filter(name="format_data_i_hora_curta")
def format_data_i_hora(datetime):
    return formats.format_data_i_hora(datetime, curt=True)


@register.filter(name="format_data_curta")
def format_data(date):
    return formats.format_data(date, curt=True)


@register.filter(name="format_data_factura")
def format_data_factura(date):
    result = formats.format_data(date, amagar_dia_setmana=True)
    if result[1] == u' ':
        result = u"0" + result
    return result


@register.filter(name="format_data_i_hora_ultra_curta")
def format_data_i_hora(datetime):
    return formats.format_data_i_hora(datetime, curt=True, amagar_any_actual=True)


@register.filter(name="format_data_ultra_curta")
def format_data(date):
    return formats.format_data(date, curt=True, amagar_any_actual=True)


@register.filter(name="format_data_dia_mes")
def format_data_dia_mes(date):
    return formats.format_data(date, curt=True, amagar_any_actual=True, amagar_dia_setmana=True)


@register.filter(name='is_checkbox')
def is_checkbox(value):
    return isinstance(value, fields.CheckboxInput)


@register.filter(name='is_date')
def is_date(value):
    return isinstance(value, fields.DateInput)


@register.filter(name='is_datetime')
def is_datetime(value):
    return isinstance(value, fields.SplitDateTimeWidget)


@register.filter(name='is_time')
def is_time(value):
    return isinstance(value, fields.TimeInput)


@register.inclusion_tag('tags/non_field_errors.html')
def non_field_errors(form):
    return {'form': form}


# only AUTH MEMBERSHIP CHECKS beyond this point ################################

def _is_member(user, group_name):
    """
    Returns True if the given User object is a member of a group with the given name.
    Please choose the names from the gestioci.settings.auth_groups constants.
    :type group_name: basestring
    :type user: django.contrib.auth.User
    """
    if isinstance(user, User):
        return user.groups.filter(name=group_name).exists()

    return False


# no volem noms de grups "literals" als templates
# @register.filter(name="is_member_of")
# def is_member_of(u, group):
#     raise DeprecationWarning
#     return u.groups.filter(name=group).exists()

@register.filter(name='is_responsable_alta')
def is_responsable_alta(u):
    return _is_member(u, auth_groups.RESPONSABLES_ALTA)


@register.filter(name='is_supervisora_alta')
def is_supervisora_alta(u):
    return _is_member(u, auth_groups.SUPERVISORES_ALTA)


@register.filter(name='is_exportador')
def is_exportador(u):
    return _is_member(u, auth_groups.EXPORTADORES)
