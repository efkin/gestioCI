# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('empreses', '0006_fix_codicomptable_uniqueness'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='empresa',
            name='nom',
        ),
    ]
