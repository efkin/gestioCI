# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('empreses', '0007_remove_empresa_nom'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='empresa',
            name='compte_corrent',
        ),
    ]
