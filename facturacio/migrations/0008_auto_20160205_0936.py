# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from decimal import Decimal


class Migration(migrations.Migration):

    dependencies = [
        ('facturacio', '0007_remove_factura_data_prevista_pagament'),
    ]

    operations = [
        migrations.AlterField(
            model_name='liniafactura',
            name='tipus_iva',
            field=models.DecimalField(default=Decimal('21.00'), verbose_name="tipus d'IVA", max_digits=4, decimal_places=2, choices=[(Decimal('0.00'), b'0 %'), (Decimal('4.00'), b'4 %'), (Decimal('10.00'), b'10 %'), (Decimal('21.00'), b'21 %')]),
        ),
    ]
