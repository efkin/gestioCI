# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projectes', '0001_initial'),
        ('empreses', '0002_auto_20150629_1815'),
        ('facturacio', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='factura',
            name='projecte',
            field=models.ForeignKey(to='projectes.ProjecteAutoocupat'),
        ),
        migrations.AddField(
            model_name='factura',
            name='proveidor',
            field=models.ForeignKey(related_name='factures_emeses', to='empreses.Empresa'),
        ),
    ]
