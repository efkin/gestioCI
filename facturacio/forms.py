# coding=utf-8

from django import forms
from django.db.models import Min, Max

from empreses.models import Cooperativa
from facturacio.models import FacturaEmesa, Trimestre, FacturaRebuda


class FormulariImportExtracteBancari(forms.Form):

    file = forms.FileField()



class BuscarEmpresaForm(forms.Form):

    nif = forms.CharField(max_length=64, required=True, label="NIF")


class FormSeleccioVolumFacturacio(forms.Form):

    cooperativa = forms.ChoiceField(required=True)
    periode = forms.ChoiceField(required=True)

    def __init__(self, *args, **kwargs):

        q = Trimestre.objects.all().aggregate(Min('data_inici'), Max('data_inici'))
        periodes = []
        for y in range(q['data_inici__min'].year, q['data_inici__max'].year + 1):
            periodes.append((str(y), str(y)))
            for q in range(1, 5):
                qn = Trimestre.nom_trimestre(y, q)
                periodes.append((qn, qn))

        periodes = (x for x in periodes)

        llista_cooperatives = Cooperativa.objects.all().order_by('nom_fiscal').values_list('id', 'nom_fiscal')

        attrs = dict(onchange='document.forms[0].submit()')

        super(FormSeleccioVolumFacturacio, self).__init__(*args, **kwargs)
        self.fields['periode'].choices = periodes
        self.fields['periode'].widget.attrs = attrs
        self.fields['cooperativa'].choices = llista_cooperatives
        self.fields['cooperativa'].widget.attrs = attrs


class FormCercarValidacioFacturaRebuda(forms.Form):

    OPCIO_DATA = 'data'
    OPCIO_PROJECTE = 'projecte'
    OPCIO_PROVEIDOR = 'proveidor'

    OPCIONS = (
        (OPCIO_DATA, u"Data de la factura"),
        (OPCIO_PROJECTE, u"Projecte autoocupat que ha rebut la factura"),
        (OPCIO_PROVEIDOR, u"Proveïdor que ha emès la factura")
    )

    cooperativa = forms.ChoiceField(required=False)
    ordenar_per = forms.ChoiceField(required=False, choices=OPCIONS)
    mostrar_estat = forms.ChoiceField(required=False, choices=FacturaRebuda.ESTATS, label=u"Estat")
    limit = forms.IntegerField(required=False, min_value=5, max_value=50, label=u"Mostrar com a màxim")
    des_de = forms.DateField(required=False)
    fins_a = forms.DateField(required=False)

    def __init__(self, *args, **kwargs):

        llista_cooperatives = Cooperativa.objects.all().order_by('nom_fiscal').values_list('id', 'nom_fiscal')

        super(FormCercarValidacioFacturaRebuda, self).__init__(*args, **kwargs)
        self.fields['cooperativa'].choices = llista_cooperatives


class FormValidacioFacturaRebuda(forms.ModelForm):

    class Meta:
        model = FacturaRebuda
        fields = ['estat', 'vist_i_plau_observacions']
        widgets = dict(vist_i_plau_observacions=forms.TextInput)

    def __init__(self, *args, **kwargs):
        super(FormValidacioFacturaRebuda, self).__init__(*args, **kwargs)
        self.fields['estat'].widget.attrs = {'class': 'do-not-selectize', 'style': 'display:none'}
        self.fields['vist_i_plau_observacions'].widget.attrs = {'onblur': 'amagar_observacions(event)'}


class FormCrearFacturaEmesa(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        clients = kwargs.pop('clients')
        super(FormCrearFacturaEmesa, self).__init__(*args, **kwargs)
        self.fields['client'].queryset = clients

    class Meta:
        model = FacturaEmesa
        fields = ['client', 'data']

    def clean_data(self):

        data = self.cleaned_data['data']

        if not Trimestre.objects.filter(obert=True, data_inici__lte=data, data_final__gte=data).exists():
            raise forms.ValidationError(u"La data ha d'estar dintre d'un dels trimestres oberts")

        return data


class FormCrearFacturaRebuda(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        proveidors = kwargs.pop('proveidors')
        super(FormCrearFacturaRebuda, self).__init__(*args, **kwargs)
        self.fields['proveidor'].queryset = proveidors

    class Meta:
        model = FacturaRebuda
        fields = ['proveidor', 'data', 'numero']

    def clean_data(self):

        data = self.cleaned_data['data']

        if not Trimestre.objects.filter(obert=True, data_inici__lte=data, data_final__gte=data).exists():
            raise forms.ValidationError(u"La data ha d'estar dintre d'un dels trimestres oberts")

        return data


class FormConfirmarFacturaEmesa(forms.ModelForm):

    class Meta:
        model = FacturaEmesa
        fields = [
            'data_venciment',
            'forma_de_pagament',
        ]


class FormEditarFacturaRebuda(forms.ModelForm):

    class Meta:
        model = FacturaRebuda
        fields = ['tipus_irpf', ]
