# coding=utf-8
import re

from datetime import timedelta, date
from decimal import Decimal

from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.db import models, transaction
from django.db.models import Max, Sum, Min
from django.utils import timezone

from projectes.models import ProjecteAutoocupat
from empreses.models import Empresa, Cooperativa
from socies.models import PersonaUsuaria, Persona
from gestioci.settings import auth_groups
from .helpers import calcular_trimestre


class QuotaTrimestral(models.Model):

    base_imposable_minima = models.DecimalField(max_digits=8, decimal_places=2)
    import_quota_trimestral = models.DecimalField(max_digits=8, decimal_places=2)
    data_vigor = models.DateField(verbose_name=u"data d'entrada en vigor")

    class Meta:
        verbose_name_plural = 'quotes trimestrals'


# TODO això hauria d'anar a "Soci" (quan es defineixi aquesta abstracció)
# o si nomes aplica a projectes autoocupats s'haurien de canviar els noms de
# QuotaTrimestral i d'aquesta funció per fer-ho palès
def trobar_quota_trimestral(data, base_imposable):

    # la taula de quotes que aplica és la que té la data d'entrada en vigor màxima tal que
    # la data d'entrada en vigor és menor o igual a la data en que es vol fer el moviment
    data_vigor = QuotaTrimestral.objects.filter(data_vigor__lte=data).aggregate(Max('data_vigor'))['data_vigor__max']

    if data_vigor is None:
        raise ValueError(u"No hi ha quotes trimestrals definides per aquesta data (%s)" % data.isoformat())

    # donada aquesta data d'entrada en vigor, trobem el tram de facturació i la quota trimestral que aplica:
    # de tots els trams per imports inferiors o iguals a la facturació que s'indica, ens quedem amb el que
    # correspon a l'import més gran
    trobar_quota = QuotaTrimestral.objects.filter(data_vigor=data_vigor)
    trobar_quota = trobar_quota.filter(base_imposable_minima__lte=base_imposable)
    trobar_quota = trobar_quota.order_by('-base_imposable_minima')

    trobar_quota = list(trobar_quota)

    if len(trobar_quota):
        return trobar_quota[0].import_quota_trimestral

    raise ValueError(u"Manca un tram amb 'base imposable minima' = 0 a 'quota trimestral' "
                     u"per 'data d'entrada en vigor' = %s" % data_vigor)


class Factura(models.Model):

    FORMA_PAGAMENT_EFECTIU = 'efectiu'
    FORMA_PAGAMENT_TRANSFERENCIA = 'transferencia'

    FORMES_DE_PAGAMENT = (
        (FORMA_PAGAMENT_EFECTIU, u"En efectiu"),
        (FORMA_PAGAMENT_TRANSFERENCIA, u"Gestiona la cooperativa"),
    )

    projecte = models.ForeignKey(ProjecteAutoocupat)

    proveidor = models.ForeignKey(Empresa, related_name='factures_emeses')
    client = models.ForeignKey(Empresa, related_name='factures_rebudes')

    data = models.DateField()

    base_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    iva_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    import_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)

    data_venciment = models.DateField(null=True, blank=True)
    forma_de_pagament = models.CharField(default=FORMA_PAGAMENT_EFECTIU, max_length=16, choices=FORMES_DE_PAGAMENT)

    def _actualitzar_camps_calculats(self):
        """
        * actualitza els camps calculats de Factura.
        * no crida a save() en acabar, pq aquesta funció s'ha de 'overload' a
        les classes que en deriven, FacturaEmesa i FacturaRebuda, i es allà a
        on s'espera que es cridi el save()
        """

        client_es_cooperativa = Cooperativa.objects.filter(pk=self.client_id).exists()
        proveidor_es_cooperativa = Cooperativa.objects.filter(pk=self.proveidor_id).exists()

        if client_es_cooperativa and proveidor_es_cooperativa:
            raise Exception(u"La factura és invàlida: el client i el proveïdor són cooperatives")

        if not (client_es_cooperativa or proveidor_es_cooperativa):
            raise Exception(u"La factura és invàlida: no implica cap cooperativa, ni com a client ni com a proveïdor")

        if not self.pertany_a_trimestre_obert():
            raise Exception(u"La factura no es pot editar: correspon a un trimestre que no esta obert")

        self.base_total = Decimal('0.00')
        self.iva_total = Decimal('0.00')
        self.import_total = Decimal('0.00')

        for l in self.liniafactura_set.all():
            self.base_total += l.base_total
            self.iva_total += l.iva_total
            self.import_total += l.import_total

    def usuari_pot_editar(self, user):

        try:
            persona = PersonaUsuaria.objects.filter(usuari=user).first().persona
            self.projecte.membres_de_referencia.get(pk=persona.pk)
            return True

        except PersonaUsuaria.DoesNotExist, Persona.DoesNotExist:
            return False

    def pertany_a_trimestre_obert(self):

        trimestre = Trimestre.trobar_trimestre(self.data)
        assert trimestre  # no s'hauria d'haver pogut crear una factura en un trimestre que no existeix

        return trimestre.obert

    def save(self, *args, **kwargs):
        if (self.client and self.client.pendent_de_revisio) or (self.proveidor and self.proveidor.pendent_de_revisio):
            raise Exception(u"La factura és invàlida: no pot implicar una empresa que no ha estat validada per GE")
        super(Factura, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = u"factures"


class SequenciaNumeroFacturaEmesa(models.Model):

    year = models.SmallIntegerField(primary_key=True)
    value = models.IntegerField()

    @staticmethod
    @transaction.atomic
    def incrementa_i_retorna_seguent_numero(data):
        sequencia = SequenciaNumeroFacturaEmesa.objects.select_for_update().get(year=data.year)
        sequencia.value += 1
        sequencia.save()
        return sequencia.value

    @staticmethod
    def crear_sequencies_necessaries():
        """
        inicialitza a 1 les seqüències per als propers anys;
        la idea és no haver d'inicialitzar la seqüència per cada any
        en el moment de generar la primera factura; com que els anys
        es creen per avançat, d'aquesta manera s'esquiva el problema
        de concurrència que requeriria un "lock" explícit que caldria
        en el supòsit que evitem d'aquesta manera.

        aquest mètode està pensat per ser cridat des d'un cron job.
        """
        years_in_advance = 5
        current_year = timezone.localtime(timezone.now()).date().year
        farthest_year = SequenciaNumeroFacturaEmesa.objects.all().aggregate(Max('year'))['year__max']
        if farthest_year is None:
            farthest_year = current_year - 1
        for year in range(farthest_year + 1, max(current_year + years_in_advance, farthest_year) + 1):
            SequenciaNumeroFacturaEmesa.objects.create(year=year, value=1)


class FacturaEmesa(Factura):

    ESTAT_EMESA = 'emesa'
    ESTAT_COBRADA = 'cobrada'

    ESTATS = (
        (ESTAT_EMESA, u"Pendent de cobrar"),
        (ESTAT_COBRADA, u"Cobrada"),
    )

    numero = models.CharField(max_length=11, blank=True, null=True)
    estat = models.CharField(max_length=16, choices=ESTATS, default=ESTAT_EMESA)
    req_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)  # recàrrec d'equivalència

    @transaction.atomic
    def assignar_numero_sequencia(self):
        n = SequenciaNumeroFacturaEmesa.incrementa_i_retorna_seguent_numero(self.data)
        self.numero = '%d/%06d' % (self.data.year, n)
        self.save()

    def save(self, *args, **kwargs):
        self._actualitzar_camps_calculats()
        self.req_total = Decimal('0.00')
        for l in self.liniafactura_set.all():
            self.req_total += l.req_total
        super(FacturaEmesa, self).save(*args, **kwargs)

    def calcular_resums(self):

        linies = self.liniafactura_set

        resum_iva = linies.values('tipus_iva').annotate(base_total=Sum('base_total'), iva_total=Sum('iva_total')).order_by('tipus_iva')
        resum_req = linies.values('tipus_req').annotate(base_total=Sum('base_total'), req_total=Sum('req_total')).order_by('tipus_req')
        resum_totals = linies.aggregate(Sum('base_total'), Sum('iva_total'), Sum('req_total'))

        import_total = sum([resum_totals[k] for k in resum_totals.keys()])
        base_total = resum_totals['base_total__sum']
        iva_total = resum_totals['iva_total__sum']
        req_total = resum_totals['req_total__sum']

        assert import_total == self.import_total
        assert base_total == self.base_total
        assert iva_total == self.iva_total
        assert req_total == self.req_total

        if len(resum_req) == 1:
            if resum_req[0]['tipus_req'] == Decimal('0.00'):
                resum_req = None

        return resum_iva, resum_req, resum_totals

    class Meta:
        verbose_name_plural = u"factures emeses"


class FacturaRebuda(Factura):

    ESTAT_REBUDA = 'rebuda'        # s'ha creat la factura al portal del soci
    ESTAT_ACCEPTADA = 'acceptada'  # s'ha rebut la factura a GE (en paper) i s'ha acceptat per la comptabilitat
    ESTAT_REBUTJADA = 'rebutjada'  # no s'ha rebut a GE i/o s'ha considerat que no és cap factura vàlida

    ESTATS = (
        (ESTAT_REBUDA, u"Pendent de revisió"),
        (ESTAT_ACCEPTADA, u"Acceptada"),
        (ESTAT_REBUTJADA, u"Rebutjada"),
    )

    numero = models.CharField(max_length=64)

    irpf_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    tipus_irpf = models.DecimalField(default=0, max_digits=4, decimal_places=2,
                                     verbose_name=u"retenció per l'IRPF",
                                     help_text=u"en tant percent")

    estat = models.CharField(max_length=16, choices=ESTATS, default=ESTAT_REBUDA)
    vist_i_plau_usuaria = models.ForeignKey(User, blank=True, null=True, related_name='+')
    vist_i_plau_data = models.DateTimeField(blank=True, null=True)
    vist_i_plau_observacions = models.TextField(blank=True)

    def save(self, *args, **kwargs):
        self._actualitzar_camps_calculats()
        self.irpf_total = ((self.base_total * self.tipus_irpf) / 100).quantize(Decimal('0.01'))
        self.import_total -= self.irpf_total
        super(FacturaRebuda, self).save(*args, **kwargs)

    def calcular_resums(self):

        linies = self.liniafactura_set

        resum_iva = linies.values('tipus_iva').annotate(base_total=Sum('base_total'), iva_total=Sum('iva_total')).order_by('tipus_iva')
        resum_totals = linies.aggregate(Sum('base_total'), Sum('iva_total'))

        import_total = sum([resum_totals[k] for k in resum_totals.keys()]) - self.irpf_total
        base_total = resum_totals['base_total__sum']
        iva_total = resum_totals['iva_total__sum']

        assert import_total == self.import_total
        assert base_total == self.base_total
        assert iva_total == self.iva_total

        return resum_iva, resum_totals

    class Meta:
        verbose_name_plural = u"factures rebudes"


class LiniaFactura(models.Model):

    IVA_0 = Decimal('0.00')
    IVA_4 = Decimal('4.00')
    IVA_10 = Decimal('10.00')
    IVA_21 = Decimal('21.00')

    TIPUS_IVA = ((IVA_0,   '0 %'),
                 (IVA_4,   '4 %'),
                 (IVA_10, '10 %'),
                 (IVA_21, '21 %')
                 )
    factura = models.ForeignKey(Factura)

    quantitat = models.DecimalField(max_digits=8, decimal_places=2)
    concepte = models.CharField(max_length=128)
    preu_unitari = models.DecimalField(max_digits=8, decimal_places=2)
    tipus_iva = models.DecimalField(max_digits=4, decimal_places=2,
                                    choices=TIPUS_IVA, default=IVA_21,
                                    verbose_name=u"tipus d'IVA")
    tipus_req = models.DecimalField(default=0, max_digits=4, decimal_places=2,
                                    verbose_name=u"tipus de recàrrec d'equivalència",
                                    help_text=u"en tant percent")

    # "calculated" fields that allow us to offset percentage and aggregation to the database layer
    base_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    iva_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    req_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    import_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)

    def save(self, *args, **kwargs):

        self.base_total = self.quantitat * self.preu_unitari
        self.iva_total = ((self.base_total * self.tipus_iva) / 100).quantize(Decimal('0.01'))
        self.req_total = ((self.base_total * self.tipus_req) / 100).quantize(Decimal('0.01'))
        self.import_total = self.base_total + self.iva_total + self.req_total

        super(LiniaFactura, self).save(*args, **kwargs)

    class Meta:
        verbose_name = u"línia de factura"
        verbose_name_plural = u"línies de factura"


class Trimestre(models.Model):

    nom = models.CharField(max_length=10, unique=True)

    data_inici = models.DateField()
    data_final = models.DateField()
    data_limit_tancament = models.DateField()

    obert = models.BooleanField(default=False)

    @property
    def tancat(self):
        return not self.obert

    @staticmethod
    def trobar_trimestre(data):
        """
        Retorna el trimestre que conté la data indicada, o None si el trimestre encara no està donat d'alta
        """

        t = Trimestre.objects.filter(data_inici__lte=data).filter(data_final__gte=data)
        if t.count():
            return t.first()

        return None

    @staticmethod
    def trobar_per_any_i_numero(year, num):
        t = Trimestre.objects.filter(nom__exact=Trimestre.nom_trimestre(year, num))
        if t.exists():
            return t[0]
        return None

    @staticmethod
    def nom_trimestre(year, num):
        return '%dT%d' % (year, num)

    @staticmethod
    def crear_trimestre(data, obert=False):
        """
        Crea i retorna el Trimestre que hi ha "al voltant" (que inclou) la data que s'especifica
        """
        year, trimestre, inici, final = calcular_trimestre(data)
        limit_tancament = final + timedelta(days=10)
        return Trimestre.objects.create(
            nom=Trimestre.nom_trimestre(year, trimestre),
            data_inici=inici,
            data_final=final,
            data_limit_tancament=limit_tancament,
            obert=obert)

    @staticmethod
    def crear_trimestres_necessaris():
        """
        Crea si és necessari el trimestre actual i el següent.
        Retorna un text amb les accions que s'han executat.
        """

        creats = []

        avui = timezone.localtime(timezone.now()).date()
        t_actual = Trimestre.trobar_trimestre(avui)
        if not t_actual:
            t_actual = Trimestre.crear_trimestre(data=avui, obert=True)
            creats.append(u"creat %s amb id %d" % (t_actual, t_actual.id))

        # la data límit de tancament d'un trimestre sempre cau dintre del següent trimestre
        d_seguent = t_actual.data_limit_tancament
        if not Trimestre.trobar_trimestre(d_seguent):
            t_seguent = Trimestre.crear_trimestre(data=d_seguent, obert=True)
            creats.append(u"creat %s amb id %d" % (t_seguent, t_seguent.id))

        if len(creats):
            return u", ".join(creats)
        else:
            return u"no s'ha fet res"

    def __unicode__(self):
        return self.nom


class Moviment(models.Model):
    """
    S'enten que:
        * els moviments són de diners (euro)
        * són sempre en el context d'un "soci" (ProjecteAutoocupat)
        * són moviments entre una Cooperativa i "una altra entitat":
            * Empresa: client o proveïdora del "soci" (ProjecteAutoocupat)
            * el mateix "soci" (ProjecteAutoocupat)
        * en qualsevol de les dues direccions:
            * import positiu: a favor de la cooperativa
            * import negatiu: a favor de l'altra entitat

    Els moviments que no fan referència a cap empresa són moviments entre
    el "soci" (ProjecteAutoocupat) i la cooperativa.
    """

    projecte = models.ForeignKey(ProjecteAutoocupat)
    cooperativa = models.ForeignKey(Cooperativa, related_name='+')
    empresa = models.ForeignKey(Empresa, null=True, blank=True, related_name='+')
    quantitat = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    data = models.DateField()
    detalls = models.TextField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, related_name='+')
    updated_by = models.ForeignKey(User, related_name='+')

    def save(self, *args, **kwargs):

        assert Cooperativa.objects.filter(pk=self.cooperativa).exists()

        # qui actualitza ha de pertànyer al grup en el moment d'actualitzar
        if not self.updated_by.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists():
            raise Exception(u"L'usuari '%s' vol crear o modificar un moviment, però no té permís." % self.updated_by)

        # qui crea l'objecte ha de pertànyer al grup en el moment de la creació
        if not self.pk:
            if not self.created_by.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists():
                raise Exception(u"L'usuari '%s' vol crear un moviment, però no té permís." % self.updated_by)

        # si el moviment es cap a o des de una empresa, aquesta no pot ser una cooperativa
        if self.empresa is not None:
            assert not Cooperativa.objects.filter(pk=self.empresa).exists()

        super(Moviment, self).save(*args, **kwargs)

#     class Meta:
#
#         abstract = True
#
#
# class MovimentEmpresa(Moviment):
#     """
#     Moviments entre una Cooperativa i una Empresa (client o proveïdora d'un "soci" (ProjecteAutoocupat))
#     """
#     empresa = models.ForeignKey(Empresa)
#
#     def save(self, *args, **kwargs):
#         assert not Cooperativa.objects.filter(pk=self.empresa).exists()
#         super(MovimentEmpresa, self).save(*args, **kwargs)
#
#     class Meta:
#         verbose_name_plural = u"Moviments Empresa"
#
#
# class MovimentProjecteAutoocupat(Moviment):
#     """
#     Moviments entre una cooperativa i un "soci" (ProjecteAutoocupat)
#     """
#
#     class Meta:
#         verbose_name = u"Moviment ProjecteAutoocupat"
#         verbose_name_plural = u"Moviments ProjecteAutoocupat"


class BalancTancamentTrimestre(models.Model):
    """
    Balanç al tancament de trimestre per un "soci" (ProjecteAutoocupat)
    """

    trimestre = models.ForeignKey(Trimestre, related_name='+')
    projecte = models.ForeignKey(ProjecteAutoocupat, related_name='+')
    # TODO afegim alguna redundància?
    # balanc_inicial = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    balanc_final = models.DecimalField(default=0, max_digits=8, decimal_places=2)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, related_name='+')
    updated_by = models.ForeignKey(User, related_name='+')

    def save(self, *args, **kwargs):

        # qui actualitza ha de pertànyer al grup en el moment d'actualitzar
        if not self.updated_by.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists():
            raise Exception(u"L'usuari '%s' vol crear o modificar un moviment, però no té permís." % self.updated_by)

        # qui crea l'objecte ha de pertànyer al grup en el moment de la creació
        if not self.pk:
            if not self.created_by.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists():
                raise Exception(u"L'usuari '%s' vol crear un moviment, però no té permís." % self.updated_by)

        super(BalancTancamentTrimestre, self).save(*args, **kwargs)

    class Meta:
        verbose_name = u"Balanç al tancament de trimestre"
        verbose_name_plural = u"Balanços al tancament de trimestre"


def trobar_ultim_trimestre_tancat():
    """
    Retorna el Trimestre que s'ha tancat més recentment, o None si no n'hi ha cap de tancat
    """
    q = Trimestre.objects.filter(obert=False).order_by('-data_limit_tancament')
    if q.exists():
        return q[0]

    return None


def trobar_balanc_actual(projecte):
    """
    Retorna una tupla amb el balanç al tancament de l'últim trimestre i el balanç actual.
    S'assumeix que el balanç al tancament de l'últim trimestre és consistent amb els
    moviments previs a la data de tancament. S'enten que els moviments fets el dia del
    tancament corresponen al següent trimestre.
    :param projecte:
    :return:
    """

    # TODO: review code and add tests or assertions to detect invalid corner cases

    # assumim que no hi ha trimestres anteriors i/o que, si n'hi ha, no estan tancats
    balanc_ultim_trimestre = None
    inici = Decimal('0.00')
    primer_dia_a_considerar = date(1970, 1, 1)

    # trobar el balanç al tancament de l'últim trimestre tancat, si n'hi ha (de trimestres tancats, i de balanços)
    ultim_trimestre_tancat = trobar_ultim_trimestre_tancat()
    if ultim_trimestre_tancat:
        primer_dia_a_considerar = ultim_trimestre_tancat.data_final
        b = BalancTancamentTrimestre.objects.filter(projecte=projecte, trimestre=ultim_trimestre_tancat)
        if b.exists():
            assert b.count() == 1
            balanc_ultim_trimestre = b.values_list('balanc_final', flat=True)[0]
            inici = balanc_ultim_trimestre

    # trobar els moviments que hi ha hagut des del tancament de l'últim trimestre
    moviments = Moviment.objects.filter(projecte=projecte)
    if balanc_ultim_trimestre is not None:
        moviments = moviments.filter(data__gte=primer_dia_a_considerar)

    import_acumulat = moviments.aggregate(Sum('quantitat')).get('quantitat__sum')
    rang_dates_moviments = moviments.aggregate(Min('data'), Max('data'))
    data_inicial = rang_dates_moviments.get('data__min')
    data_final = rang_dates_moviments.get('data__max')

    balanc_actual = inici
    if import_acumulat is not None:
        balanc_actual += import_acumulat

    return balanc_ultim_trimestre, balanc_actual, ultim_trimestre_tancat, data_inicial, data_final


class ExtracteBancari(models.Model):
    """
    It represents a snapshot of a bank statement import
    process.
    """
    nom_arxiu = models.CharField(max_length=128)
    usuaria_importadora = models.ForeignKey(User)
    data_importacio = models.DateTimeField(auto_now_add=True)


class MovimentExtracteBancari(models.Model):
    """
    It represents a single line of an imported bank statement
    """

    validator_compte_projecte = RegexValidator(regex=re.compile(r'^(COOP\d{4})?$'),
                                               message=u"el format del compte de projecte ha de ser COOP9999")
    validator_numero_factura = RegexValidator(regex=re.compile(r'^(\d{4}/\d{6})?$'),
                                              message=u"el número de factura ha de ser yyyy/999999")

    STATUS_PENDENT = 'pendent'
    STATUS_CONCILIAT = 'conciliat'
    STATUS_DESCARTAT = 'descartat'

    STATUSES = ((STATUS_PENDENT,   u"Pendent de conciliació"),
                (STATUS_CONCILIAT, u"Conciliat"),
                (STATUS_DESCARTAT, u"Descartat"))

    extracte_bancari = models.ForeignKey(ExtracteBancari)

    compte = models.CharField(max_length=64)
    tipus = models.CharField(max_length=64)
    data = models.DateField()
    concepte = models.CharField(max_length=128)
    quantitat = models.DecimalField(max_digits=8, decimal_places=2)
    compte_projecte = models.CharField(max_length=16, blank=True, validators=[validator_compte_projecte])
    numero_factura = models.CharField(max_length=64, blank=True, validators=[validator_numero_factura])
    comentaris = models.TextField(max_length=256, blank=True)
    status = models.CharField(max_length=16, default=STATUS_PENDENT, choices=STATUSES)
