# Changelog
`GestioCI` adheres to [Semantic Versioning](https://semver.org).

## 1.10.5 - 2016-02-04 - Falangista Fulminado
* Deprecated: `Factura.data_prevista_pagament` as not desired.
* Improved: Set uniqueness of `ProjecteAutoocupat.compte_ces_assignat` field.
* Deprecated: `Empresa.compte_corrent` field as unused
* Added: Import bank statements feature
* Improved: merged `Empresa.nom` field with `Empresa.nom_fiscal` field
* Added: missing STATICFILES_DIRS to production settings

## 1.4.5 - 2016-02-03 - Etarra Empoderado
* Added: docs/codenames.md
* Added: missing button to reach "validació d'empreses" feature.
* Fixed: broken corner case where there was no closed "Trimestre".
* Fixed: broken URLs redirection.
* Removed: garbage in template.

## 1.2.2 - 2016-02-02 - Dictador Destituido
* Fixed: regression in URLs redirects.
* Fixed: wrong template election in a view.

## 1.2.0 - 2016-01-31 - Camarada Concentrado
* Added: "Altres Altes" module.

## 1.1.0 - 2016-01-28 - Barrendero Bolchevique
* Improved: separe dashboard to improve user testing and minor improvements.

## 1.0.0 - 2016-01-26 - Activista Apalancado
* First release.

